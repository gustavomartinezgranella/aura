package com.gustavomartinez.greendaogenerator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class DaoAura {

    public static void main(String [] args){

        Schema schema = new Schema(1, "com.gustavomartinez.aura.Db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void addTables(final Schema schema){
        addUserEntities(schema);
        addEventEntities(schema);
        addContactEntities(schema);
        addMedicineEntities(schema);
        addTakeMedicine(schema);
    }


    private static Entity addTakeMedicine(final Schema schema){
        Entity takeMedicine = schema.addEntity("TakeMedicine");
        takeMedicine.addIdProperty().primaryKey().autoincrement();
        takeMedicine.addStringProperty("dosis").notNull();
        takeMedicine.addLongProperty("user_id").notNull();
        takeMedicine.addStringProperty("dateTake");
        return takeMedicine;
    }

    private static Entity addUserEntities(final Schema schema){
        Entity user = schema.addEntity("User");
        user.addIdProperty().primaryKey().autoincrement();
        user.addStringProperty("password").notNull();
        user.addStringProperty("email").notNull();
        user.addBooleanProperty("logged").notNull();
        user.addBooleanProperty("enable_notifications");
        return user;
    }

    private static Entity addEventEntities(final Schema schema){
        Entity event = schema.addEntity("Event");
        event.addIdProperty().primaryKey().autoincrement();
        event.addStringProperty("medicine_preventive").notNull();
        event.addStringProperty("type").notNull();
        event.addDateProperty("eventDate").notNull();
        event.addIntProperty("span_event").notNull();
        event.addStringProperty("description");
        event.addLongProperty("user_id").notNull();
        return event;
    }

    private static Entity addContactEntities(final Schema schema){
        Entity contact = schema.addEntity("Contact");
        contact.addIdProperty().primaryKey().autoincrement();
        contact.addStringProperty("name").notNull();
        contact.addStringProperty("phoneNumber").notNull();
        contact.addLongProperty("user_id").notNull();
        return contact;
    }

    private static Entity addMedicineEntities(final Schema schema){
        Entity medicine = schema.addEntity("Medicine");
        medicine.addIdProperty().primaryKey().autoincrement();
        medicine.addStringProperty("name").notNull();
        medicine.addStringProperty("dosis").notNull();
        medicine.addStringProperty("time").notNull();
        medicine.addBooleanProperty("enableNotification");
        medicine.addDateProperty("dateFrom").notNull();
        medicine.addLongProperty("user_id").notNull();
        medicine.addIntProperty("idPeriod").notNull(); //0->diariamente | 1->semanalmente | 2->cada dos dias
        return medicine;
    }
}
