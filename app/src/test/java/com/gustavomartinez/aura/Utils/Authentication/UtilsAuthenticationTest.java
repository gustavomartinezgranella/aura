package com.gustavomartinez.aura.Utils.Authentication;

import android.util.Patterns;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UtilsAuthenticationTest {

    private String email;
    private List<String> domainAllowed;
    private List<String> extersionAllowed;
    private static final String MATCH_PASSWORD = "[a-zA-Z0-9]*";
    private String password;


    @Before
    public void setUp() throws Exception {
        email = "gmartinezgranella@gmail.com";
        domainAllowed = new ArrayList<>(Arrays.asList("gmail", "yahoo", "hotmail", "outlook"));
        extersionAllowed = new ArrayList<>(Arrays.asList(".com", ".net", ".org", ".gov"));
    }

    @Test
    public void isValidEmailCorrect() {

        String extension = email.substring(email.length()-4);
        String domain = email.substring(email.indexOf("@")+1, email.length()-4);

        assertFalse(email.isEmpty());
        assertTrue(domainAllowed.contains(domain));
        assertTrue(extersionAllowed.contains(extension));
        assertTrue(email.contains("@"));
        assertTrue(Arrays.asList(email).parallelStream().allMatch(n->Character.isLetterOrDigit(n.charAt(0))));
    }

    @Test
    public void isValidEmailInCorrect() {

        email = "gmartinez granella.com";

        String extension = email.substring(email.length()-4);
        String domain = email.substring(email.indexOf("@")+1, email.length()-4);

        assertFalse(email.isEmpty() && domainAllowed.contains(domain)
                && extersionAllowed.contains(extension) && email.contains("@")
                && Arrays.asList(email).parallelStream().allMatch(n->Character.isLetterOrDigit(n.charAt(0))));
    }

    @Test
    public void isPasswordInvalidForLength(){
        password = "33jahdsjashd73e7y3w323123";
        assertFalse(password.length()==8);
    }

    @Test
    public void isPasswordInvalidForContent(){
        password = "325428*2";
        assertFalse(password.matches(MATCH_PASSWORD));
    }

    @Test
    public void isPasswordValid(){
        password = "Gam16118";
        assertFalse(password.isEmpty());
        assertTrue(password.length()==8);
        assertTrue(password.matches(MATCH_PASSWORD));
    }
}