package com.gustavomartinez.aura.Models;

import java.util.Date;

public class Medicine {

    private long id;
    private String medicine;
    private String dosis;
    private boolean enableNotification;
    private String timeDosis;
    private Date dateInitial;
    private int idUser;

    public Medicine(){}

    public Medicine(long id, String medicine, String dosis, boolean enableNotification, String timeDosis, Date dateInitial, int idUser) {
        this.id = id;
        this.medicine = medicine;
        this.dosis = dosis;
        this.enableNotification = enableNotification;
        this.timeDosis = timeDosis;
        this.dateInitial = dateInitial;
        this.idUser = idUser;
    }

    public long getId() {
        return id;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public boolean isEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(boolean enableNotification) {
        this.enableNotification = enableNotification;
    }

    public String getTimeDosis() {
        return timeDosis;
    }

    public void setTimeDosis(String timeDosis) {
        this.timeDosis = timeDosis;
    }

    public Date getDateInitial() {
        return dateInitial;
    }

    public void setDateInitial(Date dateInitial) {
        this.dateInitial = dateInitial;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
