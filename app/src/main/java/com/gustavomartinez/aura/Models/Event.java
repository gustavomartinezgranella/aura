package com.gustavomartinez.aura.Models;

public class Event {

    private long id;
    private String dateEvent;
    private String description;
    private String typeEvent;
    private String spanEvent; //duracion de la crisis
    private String medicineAntiEpilepsy;


    public Event(long id, String dateEvent, String description, String typeEvent, int spanEvent, String medicineAntiEpilepsy) {
        this.id = id;
        this.dateEvent = dateEvent;
        this.description = description;
        this.typeEvent = typeEvent;
        this.spanEvent = spanEvent+" Minutos";
        this.medicineAntiEpilepsy = medicineAntiEpilepsy;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getSpanEvent() {
        return spanEvent;
    }

    public void setSpanEvent(String spanEvent) {
        this.spanEvent = spanEvent;
    }

    public String getMedicineAntiEpilepsy() {
        return medicineAntiEpilepsy;
    }

    public void setMedicineAntiEpilepsy(String medicineAntiEpilepsy) {
        this.medicineAntiEpilepsy = medicineAntiEpilepsy;
    }
}
