package com.gustavomartinez.aura.Models;

public class TakeMedicine {

    private long id;
    private String dosis;
    private String dateTake;

    public TakeMedicine(long id, String dosis, String dateTake) {
        this.id = id;
        this.dosis = dosis;
        this.dateTake = dateTake;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getDateTake() {
        return dateTake;
    }

    public void setDateTake(String dateTake) {
        this.dateTake = dateTake;
    }
}
