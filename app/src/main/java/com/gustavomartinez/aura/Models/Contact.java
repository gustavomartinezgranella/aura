package com.gustavomartinez.aura.Models;

public class Contact {

    private long id;
    private String fullname;
    private String number;

    public Contact() {}

    public Contact(long id, String fullname, String number) {
        this.id = id;
        this.fullname = fullname;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
