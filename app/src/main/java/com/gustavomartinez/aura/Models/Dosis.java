package com.gustavomartinez.aura.Models;

public class Dosis {

    private long id;
    private String dosis;
    private String hour;

    public Dosis() {}

    public Dosis(long id, String dosis, String hour) {
        this.id = id;
        this.dosis = dosis;
        this.hour = hour;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }
}
