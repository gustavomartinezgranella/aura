package com.gustavomartinez.aura.Utils.Notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gustavomartinez.aura.Dao.Medicine;
import com.gustavomartinez.aura.Dao.User;

import java.util.List;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {

            List<com.gustavomartinez.aura.Db.User> users = User.getUserActive(context);

            if (users!= null && users.size()>0){
                List<com.gustavomartinez.aura.Models.Medicine> medicaments = Medicine.getAllMedicine(context, users.get(0).getId());

                for (com.gustavomartinez.aura.Models.Medicine medicine : medicaments){
                    if (medicine.isEnableNotification()) {
                        Utils.cancelAlarm(context, Integer.parseInt(String.valueOf(medicine.getId())));
                        Utils.setAlarm(context, medicine.getMedicine()
                                , medicine.getTimeDosis(), medicine.getDosis()
                                , medicine.getDateInitial()
                                , Integer.parseInt(String.valueOf(users.get(0).getId())));
                    }
                }
            }
        }

    }
}