package com.gustavomartinez.aura.Utils.Utilities;

import java.util.Date;

public class Util {

    public static String getDateAsString(String begin,int year, int month, int day, int offset){
        int month_selected = month+offset;
        return begin+String.valueOf((day<10)?"0"+day:day)+"/"+String.valueOf((month_selected<10)?"0"+month_selected:month_selected)+"/"+year;
    }

    public static String getTimeAsString(int hourOfDay, int minute){
        String hour = String.valueOf((hourOfDay<10)?"0"+hourOfDay:hourOfDay);
        String min = String.valueOf((minute<10)?"0"+minute:minute);
        return hour+":"+min;
    }

    public static Date getDateMedicamentToSave(String dateFull){
        int year = Integer.parseInt(dateFull.substring(dateFull.length()-4,dateFull.length()));
        int month = Integer.parseInt(dateFull.substring(dateFull.length()-7,dateFull.length()-5));
        int day = Integer.parseInt(dateFull.substring(dateFull.length()-10,dateFull.length()-8));
        Date date = new Date(year, month, day);
        return date;
    }

    public static  String getTimeDosis(String hourDosis){
        String time = hourDosis.substring(hourDosis.length()-5,hourDosis.length());
        return time;
    }
}
