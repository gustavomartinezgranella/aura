package com.gustavomartinez.aura.Utils.Messages;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.os.Looper;
import android.telephony.SmsManager;
import android.widget.Toast;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.Contact;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Localization.LocalizationProperties;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class SenderMessage {

    private Context context;
    private double latitude;
    private double longitude;

    private String SMS_SENT = "SMS_SENT";
    private String SMS_DELIVERED = "SMS_DELIVERED";
    private PendingIntent sentPendingIntent = null;
    private PendingIntent deliveredPendingIntent = null;
    private ArrayList<PendingIntent> sentPendingIntents = null;
    private ArrayList<PendingIntent> deliveredPendingIntents = null;
    private ArrayList<String> smsBodyParts = null;

    public SenderMessage(Context context) {
        this.context = context;
        this.latitude = 0;
        this.longitude = 0;
    }

    public SenderMessage(Context context, double latitude, double longitude) {
        this.context = context;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void sendMessage() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                sendMsg(context, latitude, longitude);
            }
        });
    }

    /***
     * Este metodo envia un mensaje a cada contacto de emergencia
     *
     * @param context
     * @param latitude
     * @param longitude
     */
    private void sendMsg(Context context, double latitude, double longitude) {

        long id = User.getIdUserActive(context);
        List<Contact> contacts = com.gustavomartinez.aura.Dao.Contact.getAllContacts(context, id);
        if (contacts.isEmpty()) {
            Toasty.info(context, context.getString(R.string.is_not_resource), Toast.LENGTH_SHORT).show();
        } else {

            sentPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(SMS_SENT), 0);
            deliveredPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(SMS_DELIVERED), 0);
            sentPendingIntents = new ArrayList<>();
            deliveredPendingIntents = new ArrayList<>();



            //Se envia el mensaje a cada contacto
            SmsManager manager = SmsManager.getDefault();
            String direccion = this.getAdressCrisisEpilepsy(context, latitude, longitude);

            if (direccion == null) {
                direccion = context.getString(R.string.message_warning_epilepsy_whitout_location) + " " + context.getString(R.string.tag_message);
            }

            smsBodyParts = manager.divideMessage(direccion);

            for (int i = 0; i < smsBodyParts.size(); i++) {
                sentPendingIntents.add(sentPendingIntent);
                deliveredPendingIntents.add(deliveredPendingIntent);
            }

            setBroadCasts();

            for (Contact contact : contacts) {
                manager.sendMultipartTextMessage(contact.getNumber(), null, smsBodyParts, sentPendingIntents, deliveredPendingIntents);
            }
        }
    }

    private void setBroadCasts() {
        // For when the SMS has been sent
        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toasty.info(context, context.getString(R.string.sms_sent_successfully), Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toasty.error(context, context.getString(R.string.sms_generic_failure_cause), Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toasty.error(context, context.getString(R.string.sms_service_is_unvailable), Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toasty.error(context, context.getString(R.string.sms_no_pdu_provided), Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toasty.error(context, context.getString(R.string.radio_turned_off), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SMS_SENT));

        // For when the SMS has been delivered
        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toasty.success(context, context.getString(R.string.sms_delivered), Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toasty.error(context, context.getString(R.string.sms_not_delivered), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SMS_DELIVERED));
    }


    /***
     *
     * Se obtiene la direccion a partir de un punto de coordenadas
     *
     * @param context
     * @param latitude
     * @param longitude
     * @return
     */
    private String getAdressCrisisEpilepsy(Context context, double latitude, double longitude) {
        String addressFull = null;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, LocalizationProperties.MAX_RESULT);

            if (addresses != null && addresses.size() > 0) {

                for (int index = 0; index < addresses.size(); index++) {
                    Address address = addresses.get(index);
                    addressFull = this.getMessageToSend(address.getFeatureName(), address.getSubAdminArea(),
                            address.getLocality(),address.getCountryName());
                }
                return addressFull;
            }

        } catch (IOException e) {
            Toasty.error(context, context.getString(R.string.fail_get_location), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return addressFull;
    }

    private String getMessageToSend(String streetAndNumber, String city, String province, String country){
        if (streetAndNumber!=null && province!=null && city!=null && country!=null)
            return context.getString(R.string.message_warning_epilepsy) + " " + streetAndNumber
                    + " " + province +" (" + city + ") ," + country
                    + " " + context.getString(R.string.tag_message);

        if (streetAndNumber!=null || province!=null || city!=null || country!=null)
            return context.getString(R.string.message_warning_epilepsy) + " " + (streetAndNumber!=null ? streetAndNumber : "")
                    + " " + (province!=null ? province : "") + "(" + (city!=null ? city : "Ciudad No Disponible") + ") ,"
                    + (country!=null ? country : "")
                    + " " + context.getString(R.string.tag_message);

        return context.getString(R.string.Im_not_feel)+ " " + context.getString(R.string.tag_message);
    }
}
