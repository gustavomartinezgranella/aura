package com.gustavomartinez.aura.Utils.Contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.gustavomartinez.aura.Models.Contact;

import java.util.HashSet;
import java.util.Set;

public class ContactsUtils {

    private static ContentResolver getContextResolver(Context context){
        return context.getContentResolver();
    }

    public static Cursor getCursorForInternalContacts(Context context){
        ContentResolver contentResolver = getContextResolver(context);

        return contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,new String[] {ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER
                },null,null
                ,"upper("+ContactsContract.Contacts.DISPLAY_NAME+" ) ASC");
    }

    public static Cursor getCursorForSimContacts(Context context){
        Uri uri = Uri.parse("content://icc/adn");
        Cursor cursor = getContextResolver(context).query(uri, null, null, null, null);

        if (cursor!=null)
            return cursor;
        return null;
    }

    public static Set<Contact> processInternalContacts(Context context){

        Set<Contact> contactSet= new HashSet<>();

        try {

            Cursor cursor = getCursorForInternalContacts(context);

            if(cursor!=null && cursor.getCount()>0) {
                while(cursor.moveToNext()){
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    int hasPhone = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));

                    if (hasPhone > 0) {
                        Cursor cursorPhones = getContextResolver(context).query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[] {ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);

                        while (cursorPhones.moveToNext()) {
                            String phoneNumber = cursorPhones.getString(cursorPhones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if(!isNameDigit(name) && contactSet.size()==0)
                                contactSet.add(new Contact(Long.valueOf(id),name,phoneNumber.trim()));
                            if(!isNameDigit(name) && !isOnContacts(name,phoneNumber.substring(phoneNumber.length()-3),contactSet))
                                contactSet.add(new Contact(Long.valueOf(id),name,phoneNumber.trim()));
                        }

                        cursorPhones.close();
                    }
                }
            }

            cursor.close();

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return contactSet;
    }

    public static Set<Contact> processSimContacts(Context context) {

        Set<Contact> contactSet = new HashSet<>();

        try {
            Cursor cursor = getCursorForSimContacts(context);

            if (cursor != null){

                long id;
                String name;
                String phoneNumber;
                while (cursor.moveToNext()){
                    id = Long.valueOf(cursor.getString(cursor.getColumnIndex("_id")));
                    name = cursor.getString(cursor.getColumnIndex("name"));
                    phoneNumber = cursor.getString(cursor.getColumnIndex("number")).trim();

                    if(!isNameDigit(name) && contactSet.size()==0)
                        contactSet.add(new Contact(id,name,phoneNumber));
                    if(!isNameDigit(name) && !isOnContacts(name,phoneNumber.substring(phoneNumber.length()-3),contactSet))
                        contactSet.add(new Contact(id,name,phoneNumber));
                }

                cursor.close();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return contactSet;
    }

    public static boolean isNameDigit(String name){
        return name.matches(".*\\d+.*");
    }

    private static boolean isOnContacts(String name, String lastDigitsNumber, Set<Contact> contacts){
        for (Contact contact : contacts){
            if(contact.getFullname().equals(name) && contact.getNumber().endsWith(lastDigitsNumber))
                return true;
        }

        return false;
    }
}