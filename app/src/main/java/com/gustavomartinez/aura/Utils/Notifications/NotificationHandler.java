package com.gustavomartinez.aura.Utils.Notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Permissions.Permission;


/**
 * {@link NotificationHandler} is a class for create notifications depend to android version
 */
public class NotificationHandler extends ContextWrapper {

    private NotificationManager manager;

    public static final String CHANNEL_HIGH_ID = "1";
    private final String CHANNEL_HIGH_NAME = "Canal de Alta Importancia";
    public static final String CHANNEL_LOW_ID = "2";
    private final String CHANNEL_LOW_NAME = "Canal de Baja Importancia";


    public NotificationHandler(Context context) {
        super(context);
        createChannels();
    }

    public NotificationManager getManager(){
        if (manager == null){
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return manager;
    }

    private void createChannels(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //creating high channel
            NotificationChannel channelHigh = new NotificationChannel(CHANNEL_HIGH_ID, CHANNEL_HIGH_NAME, NotificationManager.IMPORTANCE_HIGH);

            //ConfigExtras
            channelHigh.enableLights(true);
            channelHigh.setLightColor(Color.GREEN);

            channelHigh.setShowBadge(true);

            channelHigh.enableVibration(true);
            channelHigh.setVibrationPattern(new long[]{100,200,300,400,500,400,300,200,400});

            channelHigh.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            Uri defaulSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            channelHigh.setSound(defaulSoundUri, null);


            NotificationChannel channelLow = new NotificationChannel(CHANNEL_LOW_ID, CHANNEL_LOW_NAME, NotificationManager.IMPORTANCE_LOW);
            channelHigh.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);


            getManager().createNotificationChannel(channelHigh);
            getManager().createNotificationChannel(channelHigh);
        }
    }

    public Notification.Builder createNotification(String title, String message, boolean isHighImportance){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            if (isHighImportance){
                return createNotificationWithChannel(title, message, CHANNEL_HIGH_ID);
            }
            return createNotificationWithChannel(title, message, CHANNEL_LOW_ID);
        }

        return createNotificationWithOutChannel(title, message);
    }

    private Notification.Builder createNotificationWithChannel(String title, String message, String channel){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            return new Notification.Builder(getApplicationContext(), channel)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setColor(super.getColor(R.color.colorPrimary))
                    .setContentIntent(getPendingIntent(getApplicationContext(), message))
                    .setSmallIcon(R.mipmap.ic_widget_content_crisis_background)
                    .setAutoCancel(true);
        }

        return null;
    }

    private Notification.Builder createNotificationWithOutChannel(String title, String message){
        Uri defaulSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        return new Notification.Builder(getApplicationContext())
                .setContentTitle(title)
                .setContentText(message)
                .setSound(defaulSoundUri)
                .setContentIntent(getPendingIntent(getApplicationContext(), message))
                .setSmallIcon(R.mipmap.ic_widget_content_crisis_background)
                .setAutoCancel(true);
    }

    private PendingIntent getPendingIntent(Context context, String message){
        Intent intent = new Intent(context, TakeMedicamentReceiver.class);
        intent.putExtra("messageNotification", message);
        return PendingIntent.getBroadcast(context, Permission.REQUEST_CODE_TAKE_MEDICINE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
