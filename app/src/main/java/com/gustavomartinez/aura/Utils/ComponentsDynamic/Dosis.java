package com.gustavomartinez.aura.Utils.ComponentsDynamic;

import android.widget.EditText;
import android.widget.TextView;

public class Dosis {

    private Integer id;
    private TextView tv_hour;
    private EditText et_dosis;

    public Dosis(Integer id, TextView tv_hour, EditText et_dosis) {
        this.id = id;
        this.tv_hour = tv_hour;
        this.et_dosis = et_dosis;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TextView getTv_hour() {
        return tv_hour;
    }

    public void setTv_hour(TextView tv_hour) {
        this.tv_hour = tv_hour;
    }

    public EditText getEt_dosis() {
        return et_dosis;
    }

    public void setEt_dosis(EditText et_dosis) {
        this.et_dosis = et_dosis;
    }
}
