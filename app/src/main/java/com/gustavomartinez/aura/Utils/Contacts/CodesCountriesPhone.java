package com.gustavomartinez.aura.Utils.Contacts;

public class CodesCountriesPhone {

    public static String ARGENTINA = "+54";
    public static String BOLIVIA = "+591";
    public static String BRASIL = "+55";
    public static String CHILE = "+56";
    public static String CHINA = "+86";
    public static String COLOMBIA = "+57";
    public static String CUBA = "+53";
    public static String ECUADOR = "+593";
    public static String ESPANIA = "+34";
    public static String ESTADOUNIDOS = "+1";
    public static String GUATEMALA = "+502";
    public static String FRANCIA = "+33";
    public static String HONDURAS = "+504";
    public static String ITALIA = "+39";

}
