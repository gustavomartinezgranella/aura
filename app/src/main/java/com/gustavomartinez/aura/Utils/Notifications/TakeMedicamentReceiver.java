package com.gustavomartinez.aura.Utils.Notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.gustavomartinez.aura.Dao.TakeMedicine;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;

import java.util.List;

import es.dmoral.toasty.Toasty;


/**
 * TakeMedicamentReceiver es un receiver que se encarga de insertar (informar) que se a aceptado la toma de medicacion
 */
public class TakeMedicamentReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            String message = null;
            if (bundle!=null){
                message = intent.getStringExtra("messageNotification");
                //insertar toma de medicacion
                List<com.gustavomartinez.aura.Db.User> users =  User.getUserActive(context);
                if (users!=null && users.size()==1){
                    TakeMedicine.insertTakeMedicine(context, message, users.get(0).getId());
                }
            }
            Toasty.success(context, context.getString(R.string.medication_take_confirmed), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            
            Toasty.error(context, context.getString(R.string.fail_to_take_medication), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
