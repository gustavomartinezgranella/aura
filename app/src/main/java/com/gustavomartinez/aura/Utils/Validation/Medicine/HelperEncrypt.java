package com.gustavomartinez.aura.Utils.Validation.Medicine;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class HelperEncrypt {


    /***
     * decryptPassword toma un hash como entrada y retorna la password correcta a partir del hash
     *
     * @param password
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decryptPassword(String password) throws UnsupportedEncodingException {
        byte [] arrayPasswordEncrypted = Base64.decode(password, Base64.DEFAULT);
        String pass = new String(arrayPasswordEncrypted, "UTF-8");
        return pass;
    }

    /***
     * encryptPassword toma la
     * @param password
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encryptPassword(String password) throws UnsupportedEncodingException{
        byte [] arrayPasswordNoEncrypt = password.getBytes("UTF-8");
        String passEncrypted = Base64.encodeToString(arrayPasswordNoEncrypt, Base64.DEFAULT);
        return passEncrypted;
    }

}
