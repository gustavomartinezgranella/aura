package com.gustavomartinez.aura.Utils.Permissions;

public class Permission {

    public static final int INTERNET_CODE=0;
    public static final int REQUEST_PERMISSION_CAMERA =1;
    public static final int SEND_MESSAGE_AND_LOCATION_CODE=2;
    public static final int CONTACT_CODE = 3;
    public static final int CALL_PHONE_CODE = 4;
    public static final int PERMISSION_CONTACT_AND_CALL_PHONE=5;
    public static final int LOCALIZATION_FINE=6;
    public static final int REQUEST_CODE_TAKE_MEDICINE=7;
    public static final int REQUEST_PASSWORD = 8;
    public static final int IGNORE_OPTIMIZATION_REQUEST = 9;
}
