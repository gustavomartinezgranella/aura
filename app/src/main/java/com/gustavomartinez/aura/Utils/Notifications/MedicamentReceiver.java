package com.gustavomartinez.aura.Utils.Notifications;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.Random;

public class MedicamentReceiver extends BroadcastReceiver{

    public static final String ALARM_ALERT_ACTION ="com.android.alarmclock.ALARM_ALERT";
    public static final String ALARM_INTENT_EXTRA = "intent.extra.alarm";

    private NotificationHandler handler;
    private long id;
    private String tittle;
    private String message;

    /**
     * generateRandom is a method used to show different notification at UI
     * @return int
     */
    private int generateRandom(){
        Random random = new Random();
        return random.nextInt(Integer.MAX_VALUE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        if (bundle!=null){
            boolean hasImportance = true;
            id = intent.getLongExtra("ID",0);
            tittle = intent.getStringExtra("TITLE");
            message = intent.getStringExtra("MESSAGE");

            handler = new NotificationHandler(context);
            Notification.Builder nb = handler.createNotification(tittle,message,hasImportance);
            handler.getManager().notify(generateRandom(),nb.build());
        }
    }
}
