package com.gustavomartinez.aura.Utils.Authentication;

import android.content.Context;
import android.util.Patterns;

import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Validation.Medicine.HelperEncrypt;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class UtilsAuthentication {

    private static final String MATCH_PASSWORD = "[a-zA-Z0-9]*";
    public static final  String NAME_FILE_USER_PREFERENCES = "preferencesUser";

    public static boolean isValidEmail(String email){
        return !email.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(String pass) {
        boolean s = pass.matches(MATCH_PASSWORD);
        return (!pass.isEmpty() && s && pass.length()==8);
    }

    public static String elementInvalid(Context context, String input, String type){
        switch (type){
            case "email":
                if(input.isEmpty()){
                   return context.getString(R.string.register_login_entry_email);
                }else {
                    return context.getString(R.string.register_login_invalid_email);
                }
            case "password":
                if(input.isEmpty()){
                    return context.getString(R.string.register_login_entry_password);
                }else if(input.length()!=8){
                   return context.getString(R.string.register_login_overlimit);
                }else{
                    return context.getString(R.string.register_login_invalid_password);
                }
            default:
                if(input.isEmpty()){
                    return context.getString(R.string.register_login_entry_fullname);
                }else {
                    return context.getString(R.string.register_login_invalid_fullname);
                }
        }

    }
}
