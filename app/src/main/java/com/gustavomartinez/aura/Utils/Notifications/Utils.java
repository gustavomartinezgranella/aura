package com.gustavomartinez.aura.Utils.Notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.gustavomartinez.aura.R;
import java.util.Calendar;
import java.util.Date;
import static android.content.Context.ALARM_SERVICE;

public class Utils {

    private final static int delay = 7 * 1000;
    public static void setAlarm(Context context, String medicine, String hours, String dosis, Date date, int id){

        Intent i = new Intent(context, MedicamentReceiver.class);

        i.putExtra("TITLE", context.getString(R.string.tomar_medicament));
        i.putExtra("MESSAGE", medicine + " | " + hours+ " | " + dosis);

        PendingIntent pendingIntent = PendingIntent
                .getBroadcast(context.getApplicationContext(), id, i, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();

        int year = date.getYear();
        int month = date.getMonth()-1;
        int day = date.getDate();
        int hour = Integer.parseInt(hours.substring(0,2));
        int minutes = Integer.parseInt(hours.substring(hours.length()-2,hours.length()));
        calendar.set(year, month, day, hour, minutes, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        if (alarmManager != null) {

            if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
                    calendar.add(Calendar.DATE, 1);
            }

            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()+delay, pendingIntent);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);

        }
    }

    public static void cancelAlarm(Context context, int id){
        try {
            Intent i = new Intent(context, MedicamentReceiver.class);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent
                    .getBroadcast(context.getApplicationContext()
                            , id, i, PendingIntent.FLAG_CANCEL_CURRENT);
            if (alarmManager != null) {
                alarmManager.cancel(pendingIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}