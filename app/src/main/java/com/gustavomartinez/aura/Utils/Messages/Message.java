package com.gustavomartinez.aura.Utils.Messages;

import android.content.Context;
import android.location.Location;

import com.gustavomartinez.aura.Utils.Localization.LocalizationPoint;

public class Message {

    public static void sendMessage(Context context, Location lastKnownLocation, double latitud, double longitud){
        if (lastKnownLocation != null && latitud != 0.0 && longitud != 0.0){
            LocalizationPoint localizationPoint = LocalizationPoint.getLocalizationPoint();
            localizationPoint.setLongitud(longitud);
            localizationPoint.setLatitud(latitud);
            SenderMessage senderMessage = new SenderMessage(context, latitud, longitud);
            senderMessage.sendMessage();
        }
    }
}
