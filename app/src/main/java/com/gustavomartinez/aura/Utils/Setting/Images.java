package com.gustavomartinez.aura.Utils.Setting;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.gustavomartinez.aura.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

public class Images {

    public static int MAX_SIZE_IMAGE = 1440;

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        image = Bitmap.createScaledBitmap(image, width, height, true);
        return image;
    }

    public static void encodeBitMapImage(Bitmap image, SharedPreferences preferences){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos); //image is the bitmap object
        byte[] b = baos.toByteArray();
        String avatar = Base64.encodeToString(b,Base64.DEFAULT);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("avatar", avatar);
        editor.apply();
    }

    public static Bitmap decodeBitMapImage(SharedPreferences preferences){
        String avatar = preferences.getString("avatar","");
        if (!avatar.equals("")){
            byte[] imageAsBytes = Base64.decode(avatar.getBytes(), Base64.DEFAULT);

            return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        }
        return null;
    }

    public static void setImage(ImageView view, SharedPreferences preferences,Context context){

        String avatar = preferences.getString("avatar","");
        byte[] imageByteArray = Base64.decode(avatar, Base64.DEFAULT);
        if(!avatar.equals("")){
            Glide.with(context)
                    .load(imageByteArray)
                    .asBitmap()
                    .into(view);

        }else{
            view.setImageResource(R.mipmap.ic_profile);
        }
    }
}
