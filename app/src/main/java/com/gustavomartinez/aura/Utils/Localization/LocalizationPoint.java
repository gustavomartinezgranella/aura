package com.gustavomartinez.aura.Utils.Localization;

import com.google.android.gms.location.LocationServices;

public class LocalizationPoint {

    private double latitud;
    private double longitud;
    private static LocalizationPoint localizationPoint;

    private LocalizationPoint(){}

    public static LocalizationPoint getLocalizationPoint(){
        if (localizationPoint==null){
            localizationPoint = new LocalizationPoint();
        }
        return localizationPoint;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
}
