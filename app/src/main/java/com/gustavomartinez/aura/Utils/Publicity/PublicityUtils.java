package com.gustavomartinez.aura.Utils.Publicity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class PublicityUtils {

    public static void setPublicity(AdView adView){
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }


}
