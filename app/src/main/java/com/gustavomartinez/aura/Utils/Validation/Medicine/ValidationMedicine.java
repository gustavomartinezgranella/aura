package com.gustavomartinez.aura.Utils.Validation.Medicine;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.gustavomartinez.aura.Models.Dosis;
import com.gustavomartinez.aura.R;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class ValidationMedicine {

    Context context;
    Calendar calendar;

    public ValidationMedicine(Context context){
        this.context= context;
        calendar = Calendar.getInstance();
    }

    public boolean isValid(String nameMedicament, String dosis, List<com.gustavomartinez.aura.Utils.ComponentsDynamic.Dosis> listDosis) {
        if (nameMedicament.trim().isEmpty()) {
            Toasty.warning(context, context.getString(R.string.enter_name_medicine), Toast.LENGTH_SHORT).show();
            return false;
        } else {

            String matchMedicament = "[a-zA-Z][a-zA-Z ]*";
            Pattern pattern = Pattern.compile(nameMedicament);


            if (!nameMedicament.matches(matchMedicament)) {
                Toasty.warning(context, context.getString(R.string.name_medicament_contain_only_letters), Toast.LENGTH_SHORT).show();
                return false;
            }

            if (!isValidDosis(dosis, context.getString(R.string.enter_dosis)))
                return false;

            if (listDosis !=null){
                for (com.gustavomartinez.aura.Utils.ComponentsDynamic.Dosis d : listDosis){
                    if (!isValidDosis(d.getEt_dosis().getText().toString(), context.getString(R.string.enter_new_doses))){
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private boolean isValidDosis(String dosis, String message){
        if (dosis.trim().isEmpty()){
            Toasty.warning(context, message, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
