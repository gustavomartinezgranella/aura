package com.gustavomartinez.aura.Db;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "EVENT".
*/
public class EventDao extends AbstractDao<Event, Long> {

    public static final String TABLENAME = "EVENT";

    /**
     * Properties of entity Event.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Medicine_preventive = new Property(1, String.class, "medicine_preventive", false, "MEDICINE_PREVENTIVE");
        public final static Property Type = new Property(2, String.class, "type", false, "TYPE");
        public final static Property EventDate = new Property(3, java.util.Date.class, "eventDate", false, "EVENT_DATE");
        public final static Property Span_event = new Property(4, int.class, "span_event", false, "SPAN_EVENT");
        public final static Property Description = new Property(5, String.class, "description", false, "DESCRIPTION");
        public final static Property User_id = new Property(6, long.class, "user_id", false, "USER_ID");
    }


    public EventDao(DaoConfig config) {
        super(config);
    }
    
    public EventDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"EVENT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"MEDICINE_PREVENTIVE\" TEXT NOT NULL ," + // 1: medicine_preventive
                "\"TYPE\" TEXT NOT NULL ," + // 2: type
                "\"EVENT_DATE\" INTEGER NOT NULL ," + // 3: eventDate
                "\"SPAN_EVENT\" INTEGER NOT NULL ," + // 4: span_event
                "\"DESCRIPTION\" TEXT," + // 5: description
                "\"USER_ID\" INTEGER NOT NULL );"); // 6: user_id
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"EVENT\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Event entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getMedicine_preventive());
        stmt.bindString(3, entity.getType());
        stmt.bindLong(4, entity.getEventDate().getTime());
        stmt.bindLong(5, entity.getSpan_event());
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(6, description);
        }
        stmt.bindLong(7, entity.getUser_id());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Event entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getMedicine_preventive());
        stmt.bindString(3, entity.getType());
        stmt.bindLong(4, entity.getEventDate().getTime());
        stmt.bindLong(5, entity.getSpan_event());
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(6, description);
        }
        stmt.bindLong(7, entity.getUser_id());
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Event readEntity(Cursor cursor, int offset) {
        Event entity = new Event( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // medicine_preventive
            cursor.getString(offset + 2), // type
            new java.util.Date(cursor.getLong(offset + 3)), // eventDate
            cursor.getInt(offset + 4), // span_event
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // description
            cursor.getLong(offset + 6) // user_id
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Event entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setMedicine_preventive(cursor.getString(offset + 1));
        entity.setType(cursor.getString(offset + 2));
        entity.setEventDate(new java.util.Date(cursor.getLong(offset + 3)));
        entity.setSpan_event(cursor.getInt(offset + 4));
        entity.setDescription(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setUser_id(cursor.getLong(offset + 6));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Event entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Event entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Event entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
