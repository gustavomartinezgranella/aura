package com.gustavomartinez.aura.Dao;

import android.content.Context;
import android.widget.Toast;

import com.gustavomartinez.aura.Db.DaoSession;
import com.gustavomartinez.aura.Db.TakeMedicineDao;
import com.gustavomartinez.aura.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.Dao.Dao.closeConnectionWritable;

public class TakeMedicine {


    public static void insertTakeMedicine(Context context, String dosis,
                                          Long user_id){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            TakeMedicineDao takeMedicineDao = daoSession.getTakeMedicineDao();
            com.gustavomartinez.aura.Db.TakeMedicine takeMedicine = new com.gustavomartinez.aura.Db.TakeMedicine();
            takeMedicine.setDosis(dosis);
            takeMedicine.setUser_id(user_id);

            Calendar calendar = Calendar.getInstance();
            String pattern = "dd-MM-yyyy HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String dateTake = simpleDateFormat.format(calendar.getTime());

            takeMedicine.setDateTake(dateTake);
            takeMedicineDao.insert(takeMedicine);
            closeConnectionWritable();
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_take_medicine), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private static List<com.gustavomartinez.aura.Db.TakeMedicine> getTakeMedicines(Context context, long user_id){
        List<com.gustavomartinez.aura.Db.TakeMedicine> takeMedicines = null;
        try {
            DaoSession daoSession = Dao.getConnection(context);
            TakeMedicineDao takeMedicineDao = daoSession.getTakeMedicineDao();
            takeMedicines = takeMedicineDao.queryBuilder()
                    .where(TakeMedicineDao.Properties.User_id.eq(user_id))
                    .orderDesc(TakeMedicineDao.Properties.Id)
                    .list();
            closeConnectionWritable();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return takeMedicines;
    }

    public static List<com.gustavomartinez.aura.Models.TakeMedicine> getAllTakesMedicines(Context context, long user_id){
        //get the takesMedicine's list asociated to logued user
        List<com.gustavomartinez.aura.Db.TakeMedicine> takeMedicinesFromDb = getTakeMedicines(context, user_id);

        //store takesMedicine to show on UI
        List<com.gustavomartinez.aura.Models.TakeMedicine> takesMedicines = new ArrayList<>();

        for (com.gustavomartinez.aura.Db.TakeMedicine takeMedicine : takeMedicinesFromDb){

            takesMedicines.add(new com.gustavomartinez.aura.Models.TakeMedicine(
                    takeMedicine.getId(),
                    takeMedicine.getDosis(),
                    takeMedicine.getDateTake()
            ));
        }

        return takesMedicines;
    }

}
