package com.gustavomartinez.aura.Dao;

import android.content.Context;
import android.widget.Toast;

import com.gustavomartinez.aura.Db.ContactDao;
import com.gustavomartinez.aura.Db.DaoSession;
import com.gustavomartinez.aura.R;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.Dao.Dao.closeConnectionWritable;

public class Contact {


    private static List<com.gustavomartinez.aura.Db.Contact> getContacts(Context context, long user_id){
        List<com.gustavomartinez.aura.Db.Contact> contacts = null;
        try {
            DaoSession daoSession = Dao.getConnection(context);
            ContactDao contactDao = daoSession.getContactDao();
            contacts = contactDao.queryBuilder()
                    .where(ContactDao.Properties.User_id.eq(user_id))
                    .orderAsc(ContactDao.Properties.Name)
                    .list();
            closeConnectionWritable();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contacts;
    }

    public static List<com.gustavomartinez.aura.Models.Contact> getAllContacts(Context context, long user_id){
        //get the contact's list asociated to logued user
        List<com.gustavomartinez.aura.Db.Contact> contactFromDb = getContacts(context, user_id);

        //store contacts to show on UI
        List<com.gustavomartinez.aura.Models.Contact> contacts = new ArrayList<>();

        for (com.gustavomartinez.aura.Db.Contact contactDb : contactFromDb){

            contacts.add(new com.gustavomartinez.aura.Models.Contact(
                    contactDb.getId(),
                    contactDb.getName(),
                    contactDb.getPhoneNumber()
            ));
        }

        return contacts;
    }

    public static boolean isContact(Context context, String name, String phone, long id_user){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            ContactDao contactDao = daoSession.getContactDao();
            List<com.gustavomartinez.aura.Db.Contact> contacts = contactDao.queryBuilder()
                    .where(ContactDao.Properties.Name.eq(name)
                            , ContactDao.Properties.PhoneNumber.eq(phone)
                            ,ContactDao.Properties.User_id.eq(id_user)).list();
            closeConnectionWritable();
            return (contacts!=null && contacts.size()>0)?true:false;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static void insertContact(Context context, String name,
                                   String phone,
                                   Long user_id){
        try {
            if(!isContact(context,name,phone,user_id)){
                DaoSession daoSession = Dao.getConnection(context);
                ContactDao contactDao = daoSession.getContactDao();
                com.gustavomartinez.aura.Db.Contact contact = new com.gustavomartinez.aura.Db.Contact();
                contact.setName(name);
                contact.setPhoneNumber(phone);
                contact.setUser_id(user_id);
                contactDao.insert(contact);
                closeConnectionWritable();
            }
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_to_inseret_contact), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void deleteContact(Context context, long id){
        DaoSession daoSession = Dao.getConnection(context);
        ContactDao contactDao = daoSession.getContactDao();
        try{
            com.gustavomartinez.aura.Db.Contact contact = contactDao.queryBuilder()
                    .where(ContactDao.Properties.Id.eq(id))
                    .uniqueOrThrow();

            contactDao.delete(contact); //this line will delete the data entry for the id.

        } catch(Exception e) {
            // catch the exception, if there is no row available for the id.
            e.printStackTrace();
        }
    }

}
