package com.gustavomartinez.aura.Dao;

import android.content.Context;
import android.widget.Toast;

import com.gustavomartinez.aura.Db.DaoSession;
import com.gustavomartinez.aura.Db.MedicineDao;
import com.gustavomartinez.aura.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.Dao.Dao.closeConnectionWritable;

public class Medicine {


    public static void deleteMedicine(Context context, long id){
        DaoSession daoSession = Dao.getConnection(context);
        MedicineDao medicineDao = daoSession.getMedicineDao();
        try{
            com.gustavomartinez.aura.Db.Medicine medicine = medicineDao.queryBuilder()
                    .where(MedicineDao.Properties.Id.eq(id))
                    .uniqueOrThrow();

            medicineDao.delete(medicine); //this line will delete the data entry for the id.

        } catch(Exception e) {
            // catch the exception, if there is no row available for the id.
            e.printStackTrace();
        }
    }

    private static List<com.gustavomartinez.aura.Db.Medicine> getMedicines(Context context, long user_id){
        List<com.gustavomartinez.aura.Db.Medicine> medicines = null;
        try {
            DaoSession daoSession = Dao.getConnection(context);
            MedicineDao medicineDao = daoSession.getMedicineDao();
            medicines = medicineDao.queryBuilder()
                    .where(MedicineDao.Properties.User_id.eq(user_id))
                    .orderAsc(MedicineDao.Properties.Id)
                    .list();
            closeConnectionWritable();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return medicines;
    }

    public static List<com.gustavomartinez.aura.Models.Medicine> getAllMedicine(Context context, long user_id){
        //get the event's list asociated to logued user
        List<com.gustavomartinez.aura.Db.Medicine> medicineFromDb = getMedicines(context, user_id);

        //store events to show on UI
        List<com.gustavomartinez.aura.Models.Medicine> medicines = new ArrayList<>();

        if (medicineFromDb!=null){
            for (com.gustavomartinez.aura.Db.Medicine medicineDb : medicineFromDb){

                medicines.add(new com.gustavomartinez.aura.Models.Medicine(
                        medicineDb.getId(),
                        medicineDb.getName(),
                        medicineDb.getDosis(),
                        medicineDb.getEnableNotification(),
                        medicineDb.getTime(),
                        medicineDb.getDateFrom(),
                        (int) medicineDb.getUser_id()
                ));
            }
        }

        return medicines;
    }


    public static com.gustavomartinez.aura.Models.Medicine getMedicament(Context context, Long id){
        com.gustavomartinez.aura.Db.Medicine med = getMedicine(context, id);
        com.gustavomartinez.aura.Models.Medicine medicine = new com.gustavomartinez.aura.Models.Medicine();
        medicine.setDateInitial(med.getDateFrom());
        medicine.setDosis(med.getDosis());
        medicine.setEnableNotification(med.getEnableNotification());
        medicine.setId(med.getId());
        medicine.setIdUser((int)med.getUser_id());
        medicine.setMedicine(med.getName());
        medicine.setTimeDosis(med.getTime());
        return medicine;
    }

    public static com.gustavomartinez.aura.Db.Medicine getMedicine(Context context, Long id){
        DaoSession daoSession = Dao.getConnection(context);
        MedicineDao medicineDao = daoSession.getMedicineDao();

        List<com.gustavomartinez.aura.Db.Medicine> medicines = medicineDao.queryBuilder()
                .where(MedicineDao.Properties.Id.eq(id))
                .orderAsc(MedicineDao.Properties.Id)
                .list();
        closeConnectionWritable();

        if(!medicines.isEmpty())
            return medicines.get(0);
        return new com.gustavomartinez.aura.Db.Medicine();
    }

    public static void updateMedicine(Context context, String nameMedicament,
                                      String dosis,boolean enableNotification, int period, String time, Date date,
                                      Long user_id, Long idMedicine){
        try {

            com.gustavomartinez.aura.Db.Medicine medicine = getMedicine(context, idMedicine);
            DaoSession daoSession = Dao.getConnection(context);
            MedicineDao medicineDao = daoSession.getMedicineDao();
            medicine.setDateFrom(date);
            medicine.setDosis(dosis);
            medicine.setEnableNotification(enableNotification);
            medicine.setIdPeriod(period);
            medicine.setName(nameMedicament);
            medicine.setTime(time);
            medicine.setUser_id(user_id);
            medicineDao.update(medicine);
            closeConnectionWritable();
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_toinsert_medicament), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static com.gustavomartinez.aura.Db.Medicine insertMedicine(Context context, String nameMedicament,
                                      String dosis,boolean enableNotification, int period, String time, Date date,
                                      Long user_id){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            MedicineDao medicineDao = daoSession.getMedicineDao();
            com.gustavomartinez.aura.Db.Medicine medicine = new com.gustavomartinez.aura.Db.Medicine();
            medicine.setDateFrom(date);
            medicine.setDosis(dosis);
            medicine.setEnableNotification(enableNotification);
            medicine.setIdPeriod(period);
            medicine.setName(nameMedicament);
            medicine.setTime(time);
            medicine.setUser_id(user_id);
            medicineDao.insert(medicine);
            closeConnectionWritable();
            return medicine;
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_toinsert_medicament), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return null;
    }

    public static com.gustavomartinez.aura.Db.Medicine getLastMedicine(Context context){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            MedicineDao medicineDao = daoSession.getMedicineDao();

            List<com.gustavomartinez.aura.Db.Medicine> medicines = medicineDao.queryBuilder()
                    .where(MedicineDao.Properties.Id.isNotNull()).orderDesc().limit(1).list();
            closeConnectionWritable();

            if(!medicines.isEmpty())
                return medicines.get(0);

        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_toinsert_medicament), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return new com.gustavomartinez.aura.Db.Medicine();
    }


}
