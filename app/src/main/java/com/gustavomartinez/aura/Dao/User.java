package com.gustavomartinez.aura.Dao;

import android.content.Context;
import android.widget.Toast;
import com.gustavomartinez.aura.Db.DaoSession;
import com.gustavomartinez.aura.Db.UserDao;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Validation.Medicine.HelperEncrypt;

import java.io.UnsupportedEncodingException;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.Dao.Dao.closeConnectionWritable;

public class User{

    public static void insertUser(Context context,String email, String password,
                                  boolean logged, boolean enable_notifications){
        try{
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            com.gustavomartinez.aura.Db.User user = new com.gustavomartinez.aura.Db.User();
            user.setEmail(email);
            user.setPassword(HelperEncrypt.encryptPassword(password));
            user.setLogged(logged);
            user.setEnable_notifications(enable_notifications);
            userDao.insert(user);
            closeConnectionWritable();
        }catch (Exception e){
            e.printStackTrace();
            Toasty.warning(context, context.getString(R.string.register_error), Toast.LENGTH_LONG).show();
        }
    }

    public static com.gustavomartinez.aura.Db.User updatePassword(Context context, String password){
        try{

            List<com.gustavomartinez.aura.Db.User> users = getUserActive(context);
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();

            if (users.size()>0){
                com.gustavomartinez.aura.Db.User user = null;
                try {
                    user = users.get(0);
                    user.setPassword(HelperEncrypt.encryptPassword(password));
                    userDao.update(user);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return user;
            }
            closeConnectionWritable();
        }catch (Exception e){
            e.printStackTrace();
            Toasty.warning(context, context.getString(R.string.fail_to_update_pass), Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    public static List<com.gustavomartinez.aura.Db.User> getUserActive(Context context){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            List<com.gustavomartinez.aura.Db.User> usersActives = userDao.queryBuilder()
                    .where(UserDao.Properties.Logged.eq(1)).list();
            closeConnectionWritable();
            return usersActives;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static List<com.gustavomartinez.aura.Db.User> getAllUser(Context context){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            List<com.gustavomartinez.aura.Db.User> usersActives = userDao.queryBuilder()
                    .list();
            closeConnectionWritable();
            return usersActives;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static long getIdUserActive(Context context){
        long id = -1;
        List<com.gustavomartinez.aura.Db.User> users = User.getUserActive(context);
        if(users != null && (users.size()==1)){
            if(users.get(0)!=null && users.get(0).getId()!=null){
                id = users.get(0).getId();
            }
        }

        return id;
    }

    public static List<com.gustavomartinez.aura.Db.User> findUser(Context context, String email){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            List<com.gustavomartinez.aura.Db.User> usersActives = userDao.queryBuilder()
                    .where(UserDao.Properties.Email.eq(email)).list();
            closeConnectionWritable();
            return usersActives;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static com.gustavomartinez.aura.Db.User getUserByEmail(Context context, String email){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            List<com.gustavomartinez.aura.Db.User> users = userDao.queryBuilder()
                    .where(UserDao.Properties.Email.eq(email)).list();
            closeConnectionWritable();
            if (users.size()>0){
                return users.get(0);
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void changeStatusLogged(Context context, com.gustavomartinez.aura.Db.User user, boolean status){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            UserDao userDao = daoSession.getUserDao();
            user.setLogged(status);
            userDao.update(user);
            closeConnectionWritable();
        }catch (Exception e){
            e.printStackTrace();
            Toasty.error(context, context.getString(R.string.logout_user_error), Toast.LENGTH_LONG).show();
        }
    }

}
