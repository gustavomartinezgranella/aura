package com.gustavomartinez.aura.Dao;

import android.content.Context;

import com.gustavomartinez.aura.Db.DaoMaster;
import com.gustavomartinez.aura.Db.DaoSession;

import org.greenrobot.greendao.database.Database;

public class Dao {

    private final static String nameDb = "Aura";
    private static DaoSession daoSession;
    private static Database db;


    private static void openConnectionWritable(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, nameDb );
        db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public static DaoSession getConnection(Context context){
        if (daoSession == null){
            openConnectionWritable(context);
        }
        return daoSession;
    }

    public static void closeConnectionWritable(){
        daoSession = null;
        db.close();
    }

}
