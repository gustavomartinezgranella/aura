package com.gustavomartinez.aura.Dao;

import android.content.Context;
import android.widget.Toast;
import com.gustavomartinez.aura.Db.DaoSession;
import com.gustavomartinez.aura.Db.EventDao;
import com.gustavomartinez.aura.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.Dao.Dao.closeConnectionWritable;

public class Event {


    private static List<com.gustavomartinez.aura.Db.Event> getEvents(Context context, long user_id){
        List<com.gustavomartinez.aura.Db.Event> events = null;
        try {
            DaoSession daoSession = Dao.getConnection(context);
            EventDao eventDao = daoSession.getEventDao();
            events = eventDao.queryBuilder()
                    .where(EventDao.Properties.User_id.eq(user_id))
                    .orderDesc(EventDao.Properties.Id)
                    .list();
            closeConnectionWritable();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return events;
    }

    public static List<com.gustavomartinez.aura.Models.Event> getAllEvent(Context context, long user_id){
        //get the event's list asociated to logued user
        List<com.gustavomartinez.aura.Db.Event> eventFromDb = getEvents(context, user_id);

        //store events to show on UI
        List<com.gustavomartinez.aura.Models.Event> events = new ArrayList<>();

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

        for (com.gustavomartinez.aura.Db.Event eventDb : eventFromDb){

            events.add(new com.gustavomartinez.aura.Models.Event(
                    eventDb.getId(),
                    formater.format(eventDb.getEventDate()),
                    eventDb.getDescription(),
                    eventDb.getType(),
                    eventDb.getSpan_event(),
                    eventDb.getMedicine_preventive()
            ));
        }

        return events;
    }

    public static void insertEvent(Context context, String medicinePreventive,
                                   String type, int spanEvent, String description,
                                   Long usere_id){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            EventDao eventDao = daoSession.getEventDao();
            com.gustavomartinez.aura.Db.Event event = new com.gustavomartinez.aura.Db.Event();
            event.setDescription(description);
            event.setMedicine_preventive(medicinePreventive);
            event.setSpan_event(spanEvent);
            event.setType(type);
            event.setEventDate(Calendar.getInstance().getTime());
            event.setUser_id(usere_id);
            eventDao.insert(event);
            closeConnectionWritable();
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_to_insert_register), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void insertEvent(Context context, String medicinePreventive,
                                   String type, int spanEvent,
                                   Long usere_id){
        try {
            DaoSession daoSession = Dao.getConnection(context);
            EventDao eventDao = daoSession.getEventDao();
            com.gustavomartinez.aura.Db.Event event = new com.gustavomartinez.aura.Db.Event();
            event.setMedicine_preventive(medicinePreventive);
            event.setSpan_event(spanEvent);
            event.setType(type);
            event.setEventDate(Calendar.getInstance().getTime());
            event.setUser_id(usere_id);
            eventDao.insert(event);
            closeConnectionWritable();
        } catch (Exception e) {
            Toasty.error(context, context.getString(R.string.fail_to_insert_register), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void deleteEvent(Context context, long id){
            DaoSession daoSession = Dao.getConnection(context);
            EventDao eventsDao = daoSession.getEventDao();
            try{
                com.gustavomartinez.aura.Db.Event event = eventsDao.queryBuilder()
                        .where(EventDao.Properties.Id.eq(id))
                        .uniqueOrThrow();

                eventsDao.delete(event); //this line will delete the data entry for the id.

            } catch(Exception e) {
                // catch the exception, if there is no row available for the id.
                e.printStackTrace();
            }
    }
}
