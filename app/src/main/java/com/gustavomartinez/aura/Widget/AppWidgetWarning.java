package com.gustavomartinez.aura.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.UiThread;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Localization.LocalizationPoint;
import com.gustavomartinez.aura.Utils.Messages.SenderMessage;

import java.util.List;

import es.dmoral.toasty.Toasty;

import static android.support.constraint.Constraints.TAG;

/**
 * Implementation of App Widget functionality.
 */
public class AppWidgetWarning extends AppWidgetProvider {

    public static String ACTION_WIDGET_CLICK = "UPDATEWIDGET";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        super.onReceive(context, intent);

        new Handler().post(new Runnable() {
            public void run() {
                Intent intt = intent;
                String action = intt.getAction();
                Context contx = context;
                if (!action.equals(AppWidgetManager.ACTION_APPWIDGET_DELETED) && !action.equals(AppWidgetManager.ACTION_APPWIDGET_DISABLED) &&
                        !action.equals(AppWidgetManager.ACTION_APPWIDGET_ENABLED) && !action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)){
                    Log.v(TAG, "Action received = " + action);

                    List<com.gustavomartinez.aura.Db.User> users = User.getUserActive(contx);

                    boolean isLogged = (users!=null && users.size() != 0);

                    if (!isLogged) {
                        Toasty.warning(contx, contx.getString(R.string.must_login), Toast.LENGTH_SHORT).show();
                    } else {
                        LocalizationPoint localizationPoint = LocalizationPoint.getLocalizationPoint();

                        GPSTracker gpsTracker = new GPSTracker(contx);

                        if (gpsTracker.canGetLocation){
                            Log.v("UBICACION", gpsTracker.getLatitude()+","+gpsTracker.getLongitude());
                            SenderMessage message = new SenderMessage(contx, gpsTracker.getLatitude(), gpsTracker.getLongitude());
                            message.sendMessage();
                        }else {
                            SenderMessage message = new SenderMessage(contx, localizationPoint.getLatitud(), localizationPoint.getLongitud());
                            message.sendMessage();
                        }
                    }
                }
            }
        });
    }

    protected static PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, AppWidgetWarning.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        try {
            CharSequence widgetText = context.getString(R.string.appwidget_text);

            // Construct the RemoteViews object
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.app_widget_warning);
            //views.setTextViewText(R.id.appwidget_text, widgetText);
            views.setOnClickPendingIntent(R.id.appwidget_text, getPendingSelfIntent(context, ACTION_WIDGET_CLICK));
            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}