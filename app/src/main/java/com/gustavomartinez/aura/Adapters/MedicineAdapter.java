package com.gustavomartinez.aura.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gustavomartinez.aura.Models.Medicine;
import com.gustavomartinez.aura.R;

import java.util.List;

public class MedicineAdapter extends BaseAdapter{

    private Context context;
    private int layout;
    private List<Medicine> listMedicine;

    public MedicineAdapter(Context context, int layout, List<Medicine> listMedicine){
        this.layout = layout;
        this.context = context;
        this.listMedicine = listMedicine;
    }

    @Override
    public int getCount() {
        return this.listMedicine.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listMedicine.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.listMedicine.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(layout, null);
            viewHolder = new ViewHolder();

            viewHolder.nameMedicament = convertView.findViewById(R.id.nameMedicament);
            viewHolder.timeMedicament = convertView.findViewById(R.id.timeMedicament);
            viewHolder.enableNotification = convertView.findViewById(R.id.statusEnablingNotification);
            viewHolder.tvDosis = convertView.findViewById(R.id.tvDosis);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Medicine medicine = this.listMedicine.get(position);

        if (medicine!= null){
            viewHolder.enableNotification.setText((medicine.isEnableNotification())?"Notificación Activada":"Notificación Desactivada");
            viewHolder.nameMedicament.setText(new StringBuilder().append(context.getString(R.string.name)).append(medicine.getMedicine()).toString());
            viewHolder.tvDosis.setText(new StringBuilder(context.getString(R.string.dosis)).append(medicine.getDosis()));
            viewHolder.timeMedicament.setText(new StringBuilder().append(context.getString(R.string.hora)).append(medicine.getTimeDosis()).toString());

        }

        return convertView;
    }

    static class ViewHolder{
        TextView nameMedicament;
        TextView timeMedicament;
        TextView enableNotification;
        TextView tvDosis;
    }

}
