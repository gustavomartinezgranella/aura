package com.gustavomartinez.aura.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gustavomartinez.aura.Models.TakeMedicine;
import com.gustavomartinez.aura.R;

import java.util.List;

public class TakeMedicineAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<TakeMedicine> takeMedicines;

    public TakeMedicineAdapter(Context context, int layout, List<TakeMedicine> takeMedicines) {
        this.context = context;
        this.layout = layout;
        this.takeMedicines = takeMedicines;
    }

    @Override
    public int getCount() {
        return takeMedicines.size();
    }

    @Override
    public Object getItem(int i) {
        return takeMedicines.get(i);
    }

    @Override
    public long getItemId(int i) {
        return takeMedicines.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(layout, null);
            viewHolder = new ViewHolder();

            viewHolder.numberDosis = convertView.findViewById(R.id.tv_number_dosis);
            viewHolder.tvDosis = convertView.findViewById(R.id.tv_medicament);
            viewHolder.tv_hour_dosis = convertView.findViewById(R.id.tv_hour_dosis);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TakeMedicine takeMedicine = this.takeMedicines.get(position);

        if (takeMedicine!= null){
            viewHolder.numberDosis.setText(new StringBuilder().append(context.getString(R.string.dosis_n)).append(takeMedicine.getId()));
            viewHolder.tvDosis.setText(new StringBuilder().append(context.getString(R.string.take_dosis_medicine)).append(takeMedicine.getDosis()));
            viewHolder.tv_hour_dosis.setText(new StringBuilder().append(context.getString(R.string.event_date)).append(takeMedicine.getDateTake()));
        }

        return convertView;
    }

    static class ViewHolder{
        TextView numberDosis;
        TextView tvDosis;
        TextView tv_hour_dosis;
    }
}
