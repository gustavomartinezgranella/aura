package com.gustavomartinez.aura.Adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.gustavomartinez.aura.Models.Contact;
import com.gustavomartinez.aura.R;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends BaseAdapter implements Filterable{

    private Context context;
    private int layout;
    private List<Contact> contactList;
    private CustomFilter filter;

    public ContactAdapter (Context context, int layout, List<Contact> contacts){
        this.context = context;
        this.layout = layout;
        this.contactList = contacts;
    }

    @Override
    public int getCount() {
        return this.contactList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.contactList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ContactAdapter.ViewHolder viewHolder;

        if(convertView == null){
            //inflo la vista con mi layout personalizado
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(this.layout, null);
            viewHolder = new ContactAdapter.ViewHolder();

            viewHolder.fullname = convertView.findViewById(R.id.fullname);
            viewHolder.number = convertView.findViewById(R.id.phone_number);
            viewHolder.btnDeleteContact = convertView.findViewById(R.id.btnQuitContact);
            viewHolder.btnCall = convertView.findViewById(R.id.btnCall);
            /*
            * if the button is not null then register listener where it , I delete the item selected and quit it from List,
            * next register changes
            * */
            if (viewHolder.btnDeleteContact!=null){
                viewHolder.btnDeleteContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        com.gustavomartinez.aura.Dao.Contact.deleteContact(context, contactList.get(position).getId());
                        contactList.remove(contactList.get(position));
                        notifyDataSetChanged();
                    }
                });
            }

            if(viewHolder.btnCall!=null){
                viewHolder.btnCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"+contactList.get(position).getNumber()));

                        if (ActivityCompat.checkSelfPermission(context,
                                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        context.startActivity(callIntent);
                    }
                });
            }

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ContactAdapter.ViewHolder) convertView.getTag();
        }

        //nos traemos el evento actual
        Contact contact = this.contactList.get(position);

        //seteo el viewHolder
        viewHolder.fullname.setText(new StringBuilder().append(contact.getFullname()));
        viewHolder.number.setText(new StringBuilder().append("Número : ").append(contact.getNumber()));



        return convertView;
    }
    /*
    * Se define este metodo para poder recuperar el resto de la lista
    * */
    public Filter obtainFilter(List<Contact> conts){
        contactList=conts;
        return getFilter();
    }

    /*
    * @return Filter
    * */
    @Override
    public Filter getFilter() {
        if (filter == null){
            filter = new CustomFilter();
        }
        return filter;
    }

    /*
    * CustomFilter has defined for filtering by Contac's name
    * */
    class CustomFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if(constraint != null && constraint.length()>0){

                constraint = constraint.toString().toLowerCase();
                List<Contact> filters = new ArrayList<>();

                for (Contact contact: contactList){
                    if (contact.getFullname().toString().toLowerCase().contains(constraint)){
                        Contact c = new Contact(contact.getId(),contact.getFullname(), contact.getNumber());
                        filters.add(c);
                    }
                }

                filterResults.count=filters.size();
                filterResults.values=filters;
            }else{
                 filterResults.count=contactList.size();
                 filterResults.values=contactList;
            }

            return filterResults;
        }


        /*
        * set the contact's list and notify changes
        * */
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contactList = (List<Contact>) results.values;
            notifyDataSetChanged();
        }
    }

    static class ViewHolder{
        private TextView fullname;
        private TextView number;
        private Button btnDeleteContact;
        private Button btnCall;
    }
}
