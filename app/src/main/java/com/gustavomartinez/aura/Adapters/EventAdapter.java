package com.gustavomartinez.aura.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.gustavomartinez.aura.Models.Event;
import com.gustavomartinez.aura.R;

import java.util.List;

public class EventAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Event> eventList;

    public EventAdapter(Context context, int layout, List<Event> events){
        this.context = context;
        this.eventList = events;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return this.eventList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.eventList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.eventList.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if(convertView == null){
            //inflo la vista con mi layout personalizado
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(this.layout, null);
            viewHolder = new ViewHolder();

            viewHolder.numberEvent = convertView.findViewById(R.id.tv_number_event);
            viewHolder.dateEvent = convertView.findViewById(R.id.tv_date_event);
            viewHolder.spanEvent = convertView.findViewById(R.id.tv_span_event);
            viewHolder.typeEvent = convertView.findViewById(R.id.tv_type_event);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //nos traemos el evento actual
        Event event = this.eventList.get(position);

        //seteo el viewHolder
        viewHolder.spanEvent.setText(new StringBuilder().append(context.getString(R.string.event_span)).append(event.getSpanEvent()).toString());
        viewHolder.typeEvent.setText(new StringBuilder().append(context.getString(R.string.event_type)).append(event.getTypeEvent()).toString());
        viewHolder.dateEvent.setText(new StringBuilder().append(context.getString(R.string.event_date)).append(String.valueOf(event.getDateEvent())).toString());
        viewHolder.numberEvent.setText(new StringBuilder().append(context.getString(R.string.event_number)).append(String.valueOf(event.getId())).toString());

        return convertView;

    }

    static class ViewHolder{
        private TextView numberEvent;
        private TextView dateEvent;
        private TextView typeEvent;
        private TextView spanEvent;
    }
}