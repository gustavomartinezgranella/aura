package com.gustavomartinez.aura.Activities.Settings;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Activities.Event.NewEvent;
import com.gustavomartinez.aura.Activities.Home.Home;
import com.gustavomartinez.aura.Db.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;
import com.gustavomartinez.aura.Utils.Setting.Images;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;
import com.gustavomartinez.aura.Utils.Validation.Medicine.HelperEncrypt;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class SettingActivity extends AppCompatActivity {

    private final String path_photos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/AuraPhotos/";
    private File file = new File(path_photos);
    private ImageButton btnArrowBack;
    private Button btnCamera;
    private ImageView profile;
    private EditText password;
    private Button btnSavePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_setting);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        file.mkdirs();
        initComponents();
        initPassword();


        btnSavePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if(UtilsAuthentication.isPasswordValid(password.getText().toString())){
                        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        User user
                                = com.gustavomartinez.aura.Dao.User.updatePassword(SettingActivity.this, password.getText().toString());

                        if (user!=null && user.getPassword()!=null){
                            editor.putString("password", user.getPassword());
                            editor.apply();
                            password.setEnabled(false);
                            Toasty.success(SettingActivity.this, getString(R.string.password_updated), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        if(password.getText().toString().isEmpty()){
                            Toasty.error(SettingActivity.this, getString(R.string.register_login_entry_password), Toast.LENGTH_SHORT).show();
                        }else if(password.getText().toString().length()!=8){
                            Toasty.error(SettingActivity.this, getString(R.string.register_login_overlimit), Toast.LENGTH_SHORT).show();
                        }else{
                            Toasty.error(SettingActivity.this, getString(R.string.register_login_invalid_password), Toast.LENGTH_SHORT).show();
                        }
                    }


                } catch (Exception e) {
                    Toasty.error(SettingActivity.this, getString(R.string.fail_to_update_pass), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });


        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }

        });

        btnArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, Home.class);
                SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);

                String email_store = preferences.getString("email", "");
                String pass = preferences.getString("password","");
                boolean isLogged = preferences.getBoolean("isLogged",false);

                intent.putExtra("emailUser",email_store);
                intent.putExtra("pass", pass);
                intent.putExtra("isLogged", String.valueOf(isLogged));
                startActivity(intent);
            }
        });
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    private void initComponents(){
        btnArrowBack = findViewById(R.id.im_back_home);
        profile = findViewById(R.id.iv_profile);
        btnCamera = findViewById(R.id.btnTakePicture);
        password = findViewById(R.id.et_password);
        password.setEnabled(true);
        btnSavePass = findViewById(R.id.btnSavePassword);
        setImageProfile();
    }

    private void saveImageProfile(Bitmap bitmap){
        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
        if(preferences != null){
            Images.encodeBitMapImage(bitmap, preferences);
        }else{
            Toasty.error(SettingActivity.this, this.getString(R.string.cant_put_profile_image), Toast.LENGTH_SHORT).show();
        }
    }


    public void setImageProfile(){
        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
        Images.setImage(profile, preferences,SettingActivity.this);
    }

    private void initPassword(){
        List<User> users = com.gustavomartinez.aura.Dao.User.getUserActive(SettingActivity.this);
        try {
            password.setText(HelperEncrypt.decryptPassword(users.get(0).getPassword()));
        } catch (UnsupportedEncodingException e) {
            Toasty.error(SettingActivity.this, "Error al obtener datos de contacto...", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void dispatchTakePictureIntent(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            //compruebo si a aceptado, no a aceptado , o nunca se le pregunto
            if(checkPermission(Manifest.permission.CAMERA)){
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, Permission.REQUEST_PERMISSION_CAMERA);
                }
            }else{
                //no acepto o es la primera vez que se le pregunta
                if(!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                    //no se le ha preguntado aun
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, Permission.REQUEST_PERMISSION_CAMERA);
                }else{
                    Toasty.info(SettingActivity.this, this.getString(R.string.need_permission_camera), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                }
             }

        }else {
            proccessPermissionCameraWithOlderVersion();
        }
    }

    private void proccessPermissionCameraWithOlderVersion(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (checkPermission(Manifest.permission.CAMERA)) {
            startActivity(intent);
        } else {
            Toasty.info(SettingActivity.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
            intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case Permission.REQUEST_PERMISSION_CAMERA:

                String permission = permissions[0];
                int result = grantResults[0];

                if (permission.equals(Manifest.permission.CAMERA)) {

                    //COMPROBAR SI EL PERMISO ESTA DENEGADO O ACEPTADO EL PERMISO
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //concedio  el permiso
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }

                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, Permission.REQUEST_PERMISSION_CAMERA);
                        }
                    }else{
                        //no concedio el permiso
                        Toasty.info(SettingActivity.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
                    }

                }

                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean checkPermission(String permission) {
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Permission.REQUEST_PERMISSION_CAMERA && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            try {
                Bitmap bitmap = (Bitmap) extras.get("data");
                saveImageProfile(bitmap);
                profile.setImageBitmap(Images.getResizedBitmap(bitmap, Images.MAX_SIZE_IMAGE));
            }catch (Exception e){
                Log.i("SettingActivity : ", e.getMessage());
            }

        }
    }



}
