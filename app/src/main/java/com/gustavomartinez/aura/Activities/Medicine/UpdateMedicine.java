package com.gustavomartinez.aura.Activities.Medicine;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Models.Medicine;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Notifications.Utils;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;
import com.gustavomartinez.aura.Utils.Validation.Medicine.ValidationMedicine;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import es.dmoral.toasty.Toasty;
import static com.gustavomartinez.aura.R.color.colorPrimaryDark;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getDateAsString;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getDateMedicamentToSave;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getTimeAsString;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getTimeDosis;

public class UpdateMedicine extends AppCompatActivity {

    private EditText medicine;
    private EditText dosis;
    private CheckBox enableNotification;
    private TextView tv_add_hour_dosis;
    private Spinner spinner;
    private Calendar calendar;
    private TextView dateInitialMedicament;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    private String styleDosis;
    private Bundle bd;
    private AdView adView;
    private long id_user;
    private boolean enableNotificationChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_update_medicine);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle(getResources().getText(R.string.medicamentos));
            initComponents();
            setListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initComponents(){

        medicine = findViewById(R.id.update_et_medicene);
        enableNotification = findViewById(R.id.update_ch_enable_notif_medicine);
        tv_add_hour_dosis = findViewById(R.id.update_tv_add_hour_dosis);
        dosis = findViewById(R.id.update_et_add_dosis);
        calendar = Calendar.getInstance();
        dateInitialMedicament = findViewById(R.id.update_tv_date_initial_medicament);
        dateInitialMedicament
                .setText(getDateAsString(getString(R.string.comienza)
                        ,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),1));
        tv_add_hour_dosis.setText(getTimeAsString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));

        adView = findViewById(R.id.adUpdateMedicament);
        PublicityUtils.setPublicity(adView);

        spinner = findViewById(R.id.update_spinner);

        // Spinner Drop down elements
        List<String> types = new ArrayList<String>();
        types.add("Mg.");
        types.add("Ml.");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);

        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                styleDosis = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        bd = getIntent().getExtras();

        if (bd!=null){
            long id = (long) bd.get("idMedicament");
            Medicine medicament = com.gustavomartinez.aura.Dao.Medicine
                    .getMedicament(UpdateMedicine.this, id);
            medicine.setText(medicament.getMedicine());
            dateInitialMedicament
                    .setText(getDateAsString(getString(R.string.comienza)
                            ,medicament.getDateInitial().getYear(),medicament.getDateInitial().getMonth(), medicament.getDateInitial().getDate(),0));
            String type = medicament.getDosis().substring(medicament.getDosis().length()-3,medicament.getDosis().length());
            spinner.setSelection(types.indexOf(type));
            dosis.setText(medicament.getDosis().substring(0, medicament.getDosis().length()-3).trim());
            tv_add_hour_dosis.setText(medicament.getTimeDosis());
            enableNotification.setChecked(medicament.isEnableNotification());
        }

    }

    private void setListeners(){
        dateInitialMedicament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog pickerDialog = new DatePickerDialog(UpdateMedicine.this,
                        R.style.Theme_AppCompat_Dialog_Alert, mDateSetListener, year,month,day);
                pickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                pickerDialog.getWindow().setBackgroundDrawableResource(colorPrimaryDark);
                pickerDialog.show();
            }
        });

        tv_add_hour_dosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int minutes = calendar.get(Calendar.MINUTE);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                TimePickerDialog timePickerDialog = new TimePickerDialog(UpdateMedicine.this,
                        R.style.Theme_AppCompat_Dialog, mTimeSetListener, hour, minutes, true);
                timePickerDialog.getWindow().setBackgroundDrawableResource(R.color.colorPrimaryDark);
                timePickerDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener(){

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dateInitialMedicament.setText(getDateAsString(getString(R.string.comienza),year, month, dayOfMonth,1));
            }
        };

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                tv_add_hour_dosis.setText(getTimeAsString(hourOfDay, minute));
            }
        };
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    //configure button add
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_dosis, menu);
        MenuItem itemAddDosis = menu.findItem(R.id.menu_add_dosis);
        MenuItem itemDeleteDosis = menu.findItem(R.id.menu_delete_dosis);
        itemAddDosis.setVisible(false);
        itemDeleteDosis.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_save_medicament:
                ValidationMedicine validationMedicine = new ValidationMedicine(UpdateMedicine.this);
                boolean status = validationMedicine.isValid(medicine.getText().toString()
                        , dosis.getText().toString(), null);
                if (status){
                    List<com.gustavomartinez.aura.Db.User> users = com.gustavomartinez.aura.Dao.User.getUserActive(UpdateMedicine.this);

                    if(users!=null && users.size()>0){
                        try {
                            id_user = users.get(0).getId();
                            enableNotificationChecked = enableNotification.isChecked();

                            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
                            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());

                            if (!isIgnoringBatteryOptimizations){
                                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_DeviceDefault_Light_DarkActionBar));
                                builder.setTitle(R.string.warn_setting)
                                        .setMessage(getString(R.string.warn_setting))
                                        .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                                                intent.setData(Uri.parse("package:" + getPackageName()));
                                                startActivityForResult(intent, Permission.IGNORE_OPTIMIZATION_REQUEST);
                                            }
                                        })
                                        .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                updateMedicineAndAlarm(id_user, enableNotificationChecked);
                                            }
                                        })
                                        .create();
                                builder.show();
                            }else {
                                updateMedicineAndAlarm(id_user, enableNotificationChecked);
                            }
                        } catch (Exception e) {
                            Toasty.error(UpdateMedicine.this, getString(R.string.fail_insert_medicament), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }else{
                        Toasty.info(UpdateMedicine.this, getString(R.string.not_exist_profle_asociated_you_user), Toast.LENGTH_SHORT).show();
                    }
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Permission.IGNORE_OPTIMIZATION_REQUEST) {
            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (isIgnoringBatteryOptimizations)
                    updateMedicineAndAlarm(id_user, enableNotificationChecked);
            }else {
                updateMedicineAndAlarm(id_user, enableNotificationChecked);
            }
        }
    }

    private void updateMedicineAndAlarm(long id_user, boolean enableNotificationChecked) {
        if(getIntent()!=null){ //si llega es null es porque es una actualizacion
            com.gustavomartinez.aura.Dao.Medicine.updateMedicine(UpdateMedicine.this,
                    medicine.getText().toString()
                    , dosis.getText().toString()+" "+ styleDosis
                    , enableNotificationChecked
                    , 0
                    , getTimeDosis(tv_add_hour_dosis.getText().toString())
                    , getDateMedicamentToSave(dateInitialMedicament.getText().toString())
                    , id_user
                    , (long) bd.get("idMedicament"));

            if (enableNotification.isChecked()) {
                long id = (long) bd.get("idMedicament");
                Utils.cancelAlarm(UpdateMedicine.this, Integer.parseInt(String.valueOf(id)));
                Utils.setAlarm(UpdateMedicine.this, medicine.getText().toString()
                        , tv_add_hour_dosis.getText().toString(), dosis.getText().toString() + " " + styleDosis
                        ,getDateMedicamentToSave(dateInitialMedicament.getText().toString())
                        , Integer.parseInt(String.valueOf(id)));
            }else {
                long id = (long) bd.get("idMedicament");
                Utils.cancelAlarm(UpdateMedicine.this, Integer.parseInt(String.valueOf(id)));
            }

            Intent intent = new Intent(UpdateMedicine.this, ListMedicine.class);
            startActivity(intent);
        }else {
            Toasty.info(UpdateMedicine.this, getString(R.string.not_found_medicament), Toast.LENGTH_SHORT).show();
        }
    }
}
