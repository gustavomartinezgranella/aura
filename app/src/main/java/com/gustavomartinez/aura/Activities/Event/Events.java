package com.gustavomartinez.aura.Activities.Event;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Adapters.EventAdapter;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.Event;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class Events extends AppCompatActivity {

    private ListView listView;
    private List<Event> events = new ArrayList<>();
    private EventAdapter eventAdapterList;
    private AdView adView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_events);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle(getResources().getText(R.string.events));
            listView = findViewById(R.id.list_view);
            adView = findViewById(R.id.adEvents);
            PublicityUtils.setPublicity(adView);
            long id =User.getIdUserActive(Events.this);

            events = com.gustavomartinez.aura.Dao.Event.getAllEvent(Events.this, id);

            eventAdapterList = new EventAdapter(Events.this, R.layout.list_events, events);
            listView.setAdapter(eventAdapterList);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    showDetailEvent(Events.this, position);
                }
            });
            registerForContextMenu(listView);

            if (events.isEmpty())
                Toasty.info(this, getString(R.string.no_hay_registros), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showDetailEvent(Context context, int position){
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.detalle_registro)
                .setMessage(getString(R.string.num_registro)+events.get(position).getId()+"\n"
                        +getString(R.string.fecha)+events.get(position).getDateEvent()+"\n"
                        +getString(R.string.tipo_registro)+events.get(position).getTypeEvent()+"\n"
                        +getString(R.string.duracion_registro)+events.get(position).getSpanEvent()+"\n"
                        +getString(R.string.medicacion_preventiva)+events.get(position).getMedicineAntiEpilepsy()+"\n" +
                        getString(R.string.descripcion)+ ((events.get(position).getDescription())==null?getString(R.string.sin_descripcion):events.get(position).getDescription()))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    //metodos para personalizador el boton agregar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add:
                Intent intent = new Intent(Events.this, NewEvent.class);
                startActivity(intent);
                return true;
            case R.id.menu_detail_register:

                if (events.size()>0){
                    Intent intentDetailRegister = new Intent(Events.this, DetailRegister.class);
                    startActivity(intentDetailRegister);
                }else{
                    Toasty.success(Events.this, getString(R.string.no_have_events), Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(getString(R.string.register)+String.valueOf(events.get(info.position).getId()));
        menuInflater.inflate(R.menu.dialog_event,menu);
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem){

        try {
            AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

            switch (menuItem.getItemId()){
                case R.id.detalle_event:
                    try {
                        com.gustavomartinez.aura.Dao.Event.deleteEvent(this, this.events.get(menuInfo.position).getId());
                        this.events.remove(menuInfo.position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    eventAdapterList.notifyDataSetChanged();
                    return true;
                default:
                    return super.onContextItemSelected(menuItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}