package com.gustavomartinez.aura.Activities.Authentication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gustavomartinez.aura.Activities.Home.Home;
import com.gustavomartinez.aura.Activities.Register.Register;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;
import com.gustavomartinez.aura.Utils.Validation.Medicine.HelperEncrypt;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {

    //properties
    private EditText email;
    private EditText password;
    private Button register;
    private Button initSession;
    private TextView password_forget;
    private SharedPreferences preferences;

    //methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);

        //si no esta logueado
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        password_forget = findViewById(R.id.tv_password_forget);
        email = findViewById(R.id.et_email);
        password = findViewById(R.id.et_password);
        register = findViewById(R.id.btnRegister);
        initSession = findViewById(R.id.btnAccept);
        String email_user = "";

        if (getIntent() != null) {
            email_user = getIntent().getStringExtra("emailUser");
        }

        register.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this,Register.class);
            startActivity(intent);
        });


        initSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(!isFormValid(email.getText().toString(), password.getText().toString())){
                        if(!UtilsAuthentication.isValidEmail(email.getText().toString())){
                            Toasty.error(LoginActivity.this, UtilsAuthentication.elementInvalid(LoginActivity.this
                                    ,email.getText().toString(),"email"), Toast.LENGTH_LONG).show();
                        }else{
                            if (!UtilsAuthentication.isPasswordValid(password.getText().toString())){
                                Toasty.error(LoginActivity.this, UtilsAuthentication.elementInvalid(LoginActivity.this,
                                        password.getText().toString(),"password"), Toast.LENGTH_LONG).show();
                            }
                        }
                    }else{

                        List<com.gustavomartinez.aura.Db.User> users = User.findUser(
                                LoginActivity.this, email.getText().toString());

                        if (users!= null && users.size()==0){
                            Toasty.error(LoginActivity.this, LoginActivity.this.getString(R.string.login_incorrect), Toast.LENGTH_LONG).show();
                        }else{
                            try {
                                final ProgressDialog mProgress = new ProgressDialog(LoginActivity.this);
                                mProgress.setTitle(getString(R.string.authentication));
                                mProgress.setMessage(getString(R.string.wait_a_moment));
                                mProgress.setCancelable(false);
                                mProgress.setIndeterminate(true);

                                if (password.getText().toString().equals(HelperEncrypt.decryptPassword(users.get(0).getPassword()))){
                                    User.changeStatusLogged(LoginActivity.this, users.get(0), true);

                                    preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("email", users.get(0).getEmail());
                                    editor.putString("password", HelperEncrypt.decryptPassword(users.get(0).getPassword()));
                                    editor.putBoolean("isLogged", users.get(0).getLogged());
                                    editor.apply();



                                    Intent intent = new Intent(LoginActivity.this, Home.class);
                                    intent.putExtra("emailUser", users.get(0).getEmail());
                                    intent.putExtra("pass", users.get(0).getPassword());
                                    intent.putExtra("isLogged", String.valueOf(users.get(0).getLogged()));
                                    cleanForm();
                                    mProgress.show();
                                    startActivity(intent);
                                }else {
                                    Toasty.error(LoginActivity.this, "La contraseña no es correcta", Toast.LENGTH_LONG).show();
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                Toasty.error(LoginActivity.this, LoginActivity.this.getString(R.string.login_incorrect), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
            }
        });

        password_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (register.getVisibility() == View.VISIBLE){
                    Toasty.info(LoginActivity.this, getString(R.string.cant_request_password), Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(LoginActivity.this, ForgetPassword.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean isFormValid(String email, String password) throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        return (UtilsAuthentication.isValidEmail(email) && UtilsAuthentication.isPasswordValid(password));
    }

    private void cleanForm(){
        email.setText("");
        password.setText("");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }
}