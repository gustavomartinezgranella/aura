package com.gustavomartinez.aura.Activities.Authentication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Mails.Config;
import com.gustavomartinez.aura.Utils.Mails.SendMailTask;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Utils.Validation.Medicine.HelperEncrypt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import es.dmoral.toasty.Toasty;

public class ForgetPassword extends AppCompatActivity {

    private Spinner email;
    private Button requestPassword;
    private String emailSelected;
    private String phoneNumber;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            getActionBar().setDisplayShowHomeEnabled(true);
        }
        setTitle(getString(R.string.retrieve_password));

        initComponents();
    }

    public void initComponents() {
        email = findViewById(R.id.spinnerEmail);
        requestPassword = findViewById(R.id.requestPassword);
        // Spinner Drop down elements
        List<String> emails = getEmails(User.getAllUser(ForgetPassword.this));

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, emails);

        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        email.setAdapter(dataAdapter);
        email.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                emailSelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });




        requestPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!emailSelected.trim().isEmpty()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        if (ActivityCompat.checkSelfPermission(ForgetPassword.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ForgetPassword.this, new String[]{Manifest.permission.INTERNET}
                                    , Permission.REQUEST_PASSWORD);
                            Toasty.info(ForgetPassword.this, getString(R.string.enable_INTERNET), Toast.LENGTH_SHORT).show();
                        }

                        if (ActivityCompat.checkSelfPermission(ForgetPassword.this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED){
                            send();
                        }

                    }else {
                        sendPasswordForOldVersion();
                    }
                }

            }
        });

    }


    private boolean checkPermission(String permission) {
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void sendPasswordForOldVersion(){
        if (checkPermission(Manifest.permission.INTERNET)) {
            send();
        } else {
            Toasty.info(ForgetPassword.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }
    }

    public void send(){
        try {
            List toEmailList = Arrays.asList(emailSelected.split("\\s*,\\s*"));

            com.gustavomartinez.aura.Db.User user = User.getUserByEmail(ForgetPassword.this, emailSelected);

            if (user != null && user.getPassword() != null){

                password = HelperEncrypt.decryptPassword(user.getPassword());

                String message = getString(R.string.his_password_is)+password+"\n\n"
                        +getString(R.string.recomendation_aura)
                        +"\n\n"+getString(R.string.regard_team_aura);

                new SendMailTask(ForgetPassword.this).execute(Config.EMAIL,
                        Config.PASSWORD, toEmailList, getString(R.string.subject_email), message);
            }
        } catch (Exception e) {
            Toasty.error(ForgetPassword.this, getString(R.string.fail_send_email), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private List<String> getEmails(List<com.gustavomartinez.aura.Db.User> users){
        List<String> emails = new ArrayList<>();

        for (com.gustavomartinez.aura.Db.User user : users){
            emails.add(user.getEmail());
        }

        return emails;
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }
}
