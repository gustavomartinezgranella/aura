package com.gustavomartinez.aura.Activities.Contact;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Contacts.CodesCountriesPhone;
import com.gustavomartinez.aura.Utils.Contacts.ContactsUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class FactoryContact extends AppCompatActivity {

    private Spinner spinner;
    private String prefixCode;
    private EditText codeArea;
    private EditText phone;
    private Button btnSave;
    private Button btnSaveAndNewContact;
    private RadioGroup radioGroup;
    private EditText namePhone;
    private boolean isCellularPhone = false;
    private boolean isParticularPhone = false;
    private boolean isSelectedTypePhone = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factory_contact);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(getResources().getText(R.string.agregar_contacto_manualmente));

        initComponents();

        initListeners();
    }

    private void initListeners() {

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_cellphone :
                        isCellularPhone = true;
                        isParticularPhone = false;
                        isSelectedTypePhone = true;
                        break;
                    case R.id.rb_phone :
                        isCellularPhone = false;
                        isParticularPhone = true;
                        isSelectedTypePhone = true;
                        break;
                    default:
                        isCellularPhone = false;
                        isParticularPhone =false;
                        isSelectedTypePhone = false;
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (insertContact()){
                    Intent intent = new Intent(FactoryContact.this, Contacts.class);
                    startActivity(intent);
                }
            }
        });

        btnSaveAndNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (insertContact())
                    cleanForm();
            }
        });
    }

    private void cleanForm() {
        codeArea.setText("");
        namePhone.setText("");
        phone.setText("");
        radioGroup.clearCheck();
        codeArea.clearFocus();
        namePhone.clearFocus();
        phone.clearFocus();
    }

    private boolean insertContact() {
        if (isValidDataNumberPhone(namePhone.getText().toString(), codeArea.getText().toString().trim(), phone.getText().toString().trim())){
            try{
                long id_user = User.getIdUserActive(FactoryContact.this);
                String fullNumberPhone = getFullNumberPhone();
                com.gustavomartinez.aura.Dao.Contact.insertContact(FactoryContact.this
                        ,namePhone.getText().toString(),fullNumberPhone,id_user);
                return true;
            }catch(Exception e){
                Toasty.error(FactoryContact.this, getString(R.string.not_can_insert), Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
    }

    private boolean isValidDataNumberPhone(String nameContact, String code, String number) {
        if (nameContact.isEmpty() || ContactsUtils.isNameDigit(nameContact)) {
            if (nameContact.isEmpty()){
                Toasty.warning(this, getString(R.string.must_enter_name_contact), Toast.LENGTH_SHORT).show();
            }else {
                Toasty.warning(this, getString(R.string.name_should_be_only_letters), Toast.LENGTH_SHORT).show();
            }
            return false;
        }else{
            if (code.length() == 0 || code.length() > 5) {
                if (code.isEmpty()){
                    Toasty.warning(this, getString(R.string.must_enter_code_area), Toast.LENGTH_SHORT).show();
                }else {
                    Toasty.warning(this, getString(R.string.code_is_incorrect), Toast.LENGTH_SHORT).show();
                }
                return false;
            }else {
                if(!(number.length() >=8 && number.length() <= 10)){
                    Toasty.warning(this, getString(R.string.format_number_incorrect), Toast.LENGTH_SHORT).show();
                    return false;
                }else {
                    if (number.isEmpty()){
                        Toasty.warning(this, getString(R.string.must_enter_number_phone), Toast.LENGTH_SHORT).show();
                    }else {
                        if (!isSelectedTypePhone){
                            Toasty.warning(this, getString(R.string.must_select_type_phone), Toast.LENGTH_SHORT).show();
                            return false;
                        }else {
                            if (prefixCode.isEmpty()){
                                Toasty.warning(this, getString(R.string.must_select_country), Toast.LENGTH_SHORT).show();
                                return false;
                            }else {
                                if (isParticularPhone){
                                    Toasty.warning(this, getString(R.string.not_is_posible_save_particular_phone), Toast.LENGTH_LONG).show();
                                    return false;
                                }else {
                                    if (isCellularPhone){
                                        if (!(number.length() >=8 && number.length() <= 10)){
                                            Toasty.warning(this, getString(R.string.must_enter_number_phone), Toast.LENGTH_SHORT).show();
                                        }
                                        return true;
                                    }else {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    private String getFullNumberPhone() {
        String codArea = codeArea.getText().toString();
        String code = (codArea.substring(0, 1).equals("0")) ?codArea.substring(1):codArea;
        String tel = phone.getText().toString();

        if (isCellularPhone){
            if (tel.length()> 9 || tel.length()<7){
                Toasty.warning(FactoryContact.this, getString(R.string.number_entered_is_not_celular),
                        Toast.LENGTH_SHORT).show();
                return null;
            }else {
                if(tel.substring(0,2).equals("15"))
                    tel = tel.substring(2);
            }
        }

        StringBuilder numberPhone = new StringBuilder(prefixCode).append(code).append(tel);
        return numberPhone.toString();
    }

    private void setPrefixCode(String country) {

        switch (country){
            case "ARGENTINA": prefixCode = CodesCountriesPhone.ARGENTINA;
                break;
            case "BRASIL": prefixCode = CodesCountriesPhone.BRASIL;
                break;
            case "BOLIVIA": prefixCode = CodesCountriesPhone.BOLIVIA;
                break;
            case "CHILE": prefixCode = CodesCountriesPhone.CHILE;
                break;
            case "CHINA": prefixCode = CodesCountriesPhone.CHINA;
                break;
            case "COLOMBIA": prefixCode = CodesCountriesPhone.COLOMBIA;
                break;
            case "CUBA": prefixCode = CodesCountriesPhone.CUBA;
                break;
            case "ECUADOR": prefixCode = CodesCountriesPhone.ECUADOR;
                break;
            case "ESPANIA": prefixCode = CodesCountriesPhone.ESPANIA;
                break;
            case "ESTADOUNIDOS": prefixCode = CodesCountriesPhone.ESTADOUNIDOS;
                break;
            case "FRANCIA": prefixCode = CodesCountriesPhone.FRANCIA;
                break;
            case "GUATEMALA": prefixCode = CodesCountriesPhone.GUATEMALA;
                break;
            case "HONDURAS": prefixCode = CodesCountriesPhone.HONDURAS;
                break;
            default: prefixCode = CodesCountriesPhone.ITALIA;
        }
    }

    private void loadCountries(List<String> countries) {
        countries.add(getString(R.string.ar));
        countries.add(getString(R.string.bra));
        countries.add(getString(R.string.bo));
        countries.add(getString(R.string.ch));
        countries.add(getString(R.string.chi));
        countries.add(getString(R.string.col));
        countries.add(getString(R.string.cu));
        countries.add(getString(R.string.ec));
        countries.add(getString(R.string.es));
        countries.add(getString(R.string.eeuu));
        countries.add(getString(R.string.gu));
        countries.add(getString(R.string.fra));
        countries.add(getString(R.string.hon));
        countries.add(getString(R.string.it));
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    private void initComponents(){
        spinner = findViewById(R.id.sp_countries);
        codeArea = findViewById(R.id.et_code_area);
        phone = findViewById(R.id.et_phone);
        radioGroup = findViewById(R.id.rg_new_contact);
        btnSave = findViewById(R.id.btnSaveContact);
        btnSaveAndNewContact = findViewById(R.id.btnSaveAndNewContact);
        namePhone = findViewById(R.id.et_name_phone);

        // Spinner Drop down elements
        List<String> countries = new ArrayList<>();
        loadCountries(countries);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);

        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPrefixCode(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }
}