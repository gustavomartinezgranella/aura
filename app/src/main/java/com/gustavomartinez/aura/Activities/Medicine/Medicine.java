package com.gustavomartinez.aura.Activities.Medicine;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.ComponentsDynamic.Dosis;
import com.gustavomartinez.aura.Utils.Notifications.Utils;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;
import com.gustavomartinez.aura.Utils.Validation.Medicine.ValidationMedicine;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import es.dmoral.toasty.Toasty;

import static com.gustavomartinez.aura.R.color.colorPrimaryDark;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getDateAsString;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getDateMedicamentToSave;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getTimeAsString;
import static com.gustavomartinez.aura.Utils.Utilities.Util.getTimeDosis;

public class Medicine extends AppCompatActivity {

    private EditText medicine;
    private EditText dosis;
    private CheckBox enableNotification;
    private TextView tv_add_hour_dosis;
    private Spinner spinner;
    private Calendar calendar;
    private TextView dateInitialMedicament;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    private String styleDosis;
    private AdView adView;
    private LinearLayout linearLayoutContent;
    private List<View> componentsDynamic = new ArrayList<>();
    private List<Dosis> listToSave = new ArrayList<>();
    private long id_user;
    private boolean enableNotificationChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_add_medicine);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle(getResources().getText(R.string.add_medicine));
            initComponents();
            setListeners();

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initComponents(){
        medicine = findViewById(R.id.et_medicene);
        enableNotification = findViewById(R.id.ch_enable_notif_medicine);
        tv_add_hour_dosis = findViewById(R.id.tv_add_hour_dosis);
        dosis = findViewById(R.id.et_add_dosis);
        calendar = Calendar.getInstance();
        dateInitialMedicament = findViewById(R.id.tv_date_initial_medicament);
        dateInitialMedicament.setText(getDateAsString(getString(R.string.comienza),calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),1));
        tv_add_hour_dosis.setText(getTimeAsString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
        adView = findViewById(R.id.adAddMedicament);
        PublicityUtils.setPublicity(adView);
        spinner = findViewById(R.id.spinner);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        linearLayoutContent = findViewById(R.id.ll_content_add_medicaments);

        // Spinner Drop down elements
        List<String> type = new ArrayList<>();
        type.add("Mg.");
        type.add("Ml.");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, type);

        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                styleDosis = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setListeners(){
        dateInitialMedicament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog pickerDialog = new DatePickerDialog(Medicine.this,
                        R.style.Theme_AppCompat_Dialog_Alert, mDateSetListener, year,month,day);
                pickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                pickerDialog.getWindow().setBackgroundDrawableResource(colorPrimaryDark);
                pickerDialog.show();
            }
        });

        tv_add_hour_dosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int minutes = calendar.get(Calendar.MINUTE);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Medicine.this,
                        R.style.Theme_AppCompat_Dialog, mTimeSetListener, hour, minutes, true);
                timePickerDialog.getWindow().setBackgroundDrawableResource(R.color.colorPrimaryDark);
                timePickerDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener(){

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dateInitialMedicament.setText(getDateAsString(getString(R.string.comienza),year, month, dayOfMonth,1));
            }
        };

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                tv_add_hour_dosis.setText(getTimeAsString(hourOfDay, minute));
            }
        };
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    //configure button add
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_dosis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_save_medicament:
                ValidationMedicine validationMedicine = new ValidationMedicine(Medicine.this);
                boolean status = validationMedicine.isValid(medicine.getText().toString()
                                , dosis.getText().toString(), listToSave);
                if (status){
                    List<com.gustavomartinez.aura.Db.User> users = com.gustavomartinez.aura.Dao.User.getUserActive(Medicine.this);

                    if(users!=null && users.size()>0) {
                        try {
                            id_user = users.get(0).getId();
                            enableNotificationChecked = enableNotification.isChecked();

                            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
                            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());

                            if (!isIgnoringBatteryOptimizations){
                                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_DeviceDefault_Light_DarkActionBar));
                                builder.setTitle(R.string.warn_setting)
                                        .setMessage(getString(R.string.warn_setting))
                                        .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                                                intent.setData(Uri.parse("package:" + getPackageName()));
                                                startActivityForResult(intent, Permission.IGNORE_OPTIMIZATION_REQUEST);
                                            }
                                        })
                                        .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                createMedicineAndAlarm(id_user, enableNotificationChecked);
                                            }
                                        })
                                        .create();
                                builder.show();
                            }else {
                                createMedicineAndAlarm(id_user, enableNotificationChecked);
                            }
                        } catch (Exception e) {
                            Toasty.error(Medicine.this, getString(R.string.fail_insert_medicament), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    } else{
                        Toasty.info(Medicine.this, getString(R.string.not_exist_profle_asociated_you_user), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.menu_add_dosis:
                LinearLayout linearContent = new LinearLayout(Medicine.this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                TextView tv_hour = new TextView(Medicine.this);
                TimePickerDialog.OnTimeSetListener mTs = null;
                EditText et_dosis = new EditText(Medicine.this);

                TextView finalTv_hour = tv_hour;
                mTs = (view, hourOfDay, minute) -> finalTv_hour.setText(getTimeAsString(hourOfDay, minute));

                initComponentsDynamic(linearContent, layoutParams, tv_hour, mTs, et_dosis);

                if (linearContent != null){
                    List<Dosis> views = new ArrayList<>();
                    views.add(new Dosis(linearContent.getId(),tv_hour, et_dosis));
                    addComponents(linearContent, views);
                    componentsDynamic.add(linearContent);
                    listToSave.add(new Dosis(linearContent.getId(),tv_hour, et_dosis));
                }

                if (linearLayoutContent!=null)
                    linearLayoutContent.addView(linearContent);
                break;
            case R.id.menu_delete_dosis:
                if (componentsDynamic.size()>0){
                    View view = componentsDynamic.get(componentsDynamic.size()-1);
                    linearLayoutContent.removeView(view);
                    componentsDynamic.remove(view);

                    for (Dosis dosis : listToSave){
                        if (dosis.getId() == view.getId()){
                            listToSave.remove(dosis);
                        }
                    }
                }

                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void createMedicineAndAlarm(long id_user, boolean enableNotificationChecked) {
        com.gustavomartinez.aura.Db.Medicine med = insertMedicine(Medicine.this,medicine.getText().toString(), dosis.getText().toString(), enableNotificationChecked,
                tv_add_hour_dosis.getText().toString(), dateInitialMedicament.getText().toString(), id_user);

        insertAlarm(Medicine.this, medicine.getText().toString(), med, tv_add_hour_dosis.getText().toString(),
                dosis.getText().toString(), dateInitialMedicament.getText().toString());

        for (Dosis dosis : listToSave){
            insertMedicine(Medicine.this,medicine.getText().toString(), dosis.getEt_dosis().getText().toString(), enableNotificationChecked,
                    dosis.getTv_hour().getText().toString(), dateInitialMedicament.getText().toString(), id_user);
            insertAlarm(Medicine.this, medicine.getText().toString(), med, dosis.getTv_hour().getText().toString(), dosis.getEt_dosis().getText().toString(),
                    dateInitialMedicament.getText().toString());
        }

        Intent intent = new Intent(Medicine.this, ListMedicine.class);
        startActivity(intent);
    }

    private void addComponents(LinearLayout linearContent, List<Dosis> views) {
        for (Dosis dosis : views){
            linearContent.addView(dosis.getTv_hour());
            linearContent.addView(dosis.getEt_dosis());
        }
    }

    public com.gustavomartinez.aura.Db.Medicine insertMedicine(Context context, String medicine, String dosis, boolean enableNotificationChecked,
                               String hour_dosis, String dateInitialMedicament, long id_user){
        com.gustavomartinez.aura.Db.Medicine med = com.gustavomartinez.aura.Dao.Medicine.insertMedicine(
                context,
                medicine,
                dosis + " " + styleDosis
                , enableNotificationChecked
                , 0
                , getTimeDosis(hour_dosis)
                , getDateMedicamentToSave(dateInitialMedicament)
                , id_user);

        return med;
    }

    public void insertAlarm(Context context, String medicine, com.gustavomartinez.aura.Db.Medicine med,
                            String hour_dosis,String dosis, String dateInitialMedicament){
        if (enableNotification.isChecked()) {
            int id = Integer.parseInt(med.getId().toString());
            Utils.setAlarm(context, medicine, hour_dosis, dosis+ " " + styleDosis
                    ,getDateMedicamentToSave(dateInitialMedicament)
                    , id);
        }
    }


    private void initComponentsDynamic(LinearLayout linearContent, LinearLayout.LayoutParams layoutParams, TextView tv_hour
            , TimePickerDialog.OnTimeSetListener mTs, EditText et_dosis) {
        linearContent.setOrientation(LinearLayout.VERTICAL);
        layoutParams.setMargins(20, 20, 20, 20);
        Random rm = new Random();

        linearContent.setId(rm.nextInt(1000));

        tv_hour.setTextSize(25);
        tv_hour.setText(getTimeAsString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
        tv_hour.setLayoutParams(layoutParams);
        tv_hour.setPaddingRelative(20,0,20,0);

        tv_hour.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int minutes = calendar.get(Calendar.MINUTE);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            TimePickerDialog timePickerDialog = new TimePickerDialog(Medicine.this,
                    R.style.Theme_AppCompat_Dialog, mTs, hour, minutes, true);
            timePickerDialog.getWindow().setBackgroundDrawableResource(R.color.colorPrimaryDark);
            timePickerDialog.show();
        });

        et_dosis.setHint(getString(R.string.enter_dosis));
        et_dosis.setLayoutParams(layoutParams);
        et_dosis.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_dosis.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
        et_dosis.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Permission.IGNORE_OPTIMIZATION_REQUEST) {
            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (isIgnoringBatteryOptimizations)
                    createMedicineAndAlarm(id_user, enableNotificationChecked);
            }else {
                createMedicineAndAlarm(id_user, enableNotificationChecked);
            }
        }
    }
}