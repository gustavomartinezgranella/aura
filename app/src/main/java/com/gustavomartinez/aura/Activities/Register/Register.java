package com.gustavomartinez.aura.Activities.Register;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gustavomartinez.aura.Activities.Home.Home;
import com.gustavomartinez.aura.Activities.Authentication.LoginActivity;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import es.dmoral.toasty.Toasty;


public class Register extends AppCompatActivity {

    //properties
    private EditText email;
    private EditText password;
    private Button accept;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register);

        email = findViewById(R.id.et_email);
        password = findViewById(R.id.et_password);
        accept = findViewById(R.id.btnRegister);
        cancel = findViewById(R.id.btnCancell);

        final ProgressDialog mProgress = new ProgressDialog(this);
        mProgress.setTitle(getString(R.string.create_account));
        mProgress.setMessage(getString(R.string.wait_a_moment));
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!isFormValid(email.getText().toString(), password.getText().toString())){
                        if(!UtilsAuthentication.isValidEmail(email.getText().toString())){
                            Toasty.warning(Register.this, UtilsAuthentication.elementInvalid(Register.this, email.getText().toString(),"email"), Toast.LENGTH_LONG).show();
                        }else{
                            try {
                                if (!UtilsAuthentication.isPasswordValid(password.getText().toString())){
                                    Toasty.error(Register.this, UtilsAuthentication.elementInvalid(Register.this, password.getText().toString(),"password"), Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }else{
                        com.gustavomartinez.aura.Db.User user = com.gustavomartinez.aura.Dao.User.getUserByEmail(Register.this,email.getText().toString());
                        if (user!=null){
                            Toasty.info(Register.this, Register.this.getString(R.string.user_already_exist), Toast.LENGTH_LONG).show();
                        }else{

                            try {
                                com.gustavomartinez.aura.Dao.User.insertUser(Register.this,email.getText().toString(), password.getText().toString(),
                                true, true);
                                user = com.gustavomartinez.aura.Dao.User.getUserByEmail(Register.this,email.getText().toString());
                                SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("email", user.getEmail());
                                editor.putString("password", user.getPassword());
                                editor.putBoolean("isLogged", user.getLogged());
                                editor.apply();

                                Intent intent = new Intent(Register.this, Home.class);
                                intent.putExtra("emailUser", user.getEmail());
                                intent.putExtra("pass", user.getPassword());
                                intent.putExtra("isLogged", String.valueOf(user.getLogged()));
                                mProgress.show();
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this,LoginActivity.class);
                startActivity(intent);
            }
        });


    }

    private boolean isFormValid(String email, String password) throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        return (UtilsAuthentication.isValidEmail(email) && UtilsAuthentication.isPasswordValid(password));
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }
}
