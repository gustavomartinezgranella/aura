package com.gustavomartinez.aura.Activities.Medicine;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Adapters.MedicineAdapter;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.Medicine;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Notifications.Utils;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ListMedicine extends AppCompatActivity {

    private ListView listView;
    private List<Medicine> medicines = new ArrayList<>();
    private MedicineAdapter medicineAdapter;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_medicine);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle(getResources().getText(R.string.medicamentos));
            listView = findViewById(R.id.listview_medicine);
            adView = findViewById(R.id.adListMedicine);
            PublicityUtils.setPublicity(adView);
            long id = User.getIdUserActive(ListMedicine.this);

            medicines = com.gustavomartinez.aura.Dao.Medicine.getAllMedicine(ListMedicine.this, id);

            medicineAdapter = new MedicineAdapter(ListMedicine.this, R.layout.list_medicine, medicines);
            listView.setAdapter(medicineAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(ListMedicine.this, UpdateMedicine.class);
                    long ids = medicines.get(position).getId();
                    intent.putExtra("idMedicament", ids);
                    startActivity(intent);
                }
            });
            registerForContextMenu(listView);

            if (medicines.isEmpty())
                Toasty.info(this, getString(R.string.no_hay_medicamentos), Toast.LENGTH_SHORT).show();

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }


    //metodos para personalizador el boton agregar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.medicine_menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.medicine_menu_add:
                Intent intent = new Intent(ListMedicine.this, com.gustavomartinez.aura.Activities.Medicine.Medicine.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        SpannableString tittle = new SpannableString(getString(R.string.medicine)+String.valueOf(medicines.get(info.position).getId()));
        tittle.setSpan(new ForegroundColorSpan(Color.BLACK), 0, tittle.length(), 0);
        menu.setHeaderTitle(tittle);
        menuInflater.inflate(R.menu.dialog_medicine,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem){

        try {
            AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

            switch (menuItem.getItemId()){
                case R.id.detalle_medicine:
                    try {
                        com.gustavomartinez.aura.Dao.Medicine.deleteMedicine(ListMedicine.this
                                , this.medicines.get(menuInfo.position).getId());
                        int id = Integer.parseInt(String.valueOf(this.medicines.get(menuInfo.position).getId()));
                        this.medicines.remove(menuInfo.position);
                        Utils.cancelAlarm(ListMedicine.this, id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    medicineAdapter.notifyDataSetChanged();
                    return true;
                default:
                    return super.onContextItemSelected(menuItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }


}
