package com.gustavomartinez.aura.Activities.Event;

import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.Event;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class DetailRegister extends AppCompatActivity {

    private AdView adView;
    private List<Event> events = new ArrayList<>();
    private LinearLayout linearLayoutContent;
    private Spinner spinner;
    private int cantDiasFrom = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail_register);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle("Detalle de Registros");
            adView = findViewById(R.id.adDetailRegister);
            PublicityUtils.setPublicity(adView);

            PieChart pieChart = findViewById(R.id.pieChart);
            long id =User.getIdUserActive(DetailRegister.this);

            linearLayoutContent = findViewById(R.id.detailRegister);
            events = com.gustavomartinez.aura.Dao.Event.getAllEvent(DetailRegister.this, id);

            if (events.size()>0){

                int countAura=0;
                int countConvulsion=0;
                int countContactRupture=0;

                final String AURA = getString(R.string.aura_crisis_simple);
                final String CONVULSION = getString(R.string.convulsi_n_t_nico_cl_nica);
                final String RUPTURE = getString(R.string.ruptura_de_contacto_crisis_compleja);

                for (Event event : events) {

                    if (event.getTypeEvent().equals(AURA)){
                        countAura++;
                    }

                    if (event.getTypeEvent().equals(CONVULSION)){
                        countConvulsion++;
                    }

                    if (event.getTypeEvent().equals(RUPTURE)){
                        countContactRupture++;
                    }

                }

                //definimos algunos atributos
                pieChart.setHoleRadius(40f);
                pieChart.setRotationEnabled(true);
                pieChart.animateXY(1500,1500);
                pieChart.setCenterTextColor(R.color.colorAccent);

                //creamos una lista de valores Y
                ArrayList<Entry> valsY = new ArrayList<Entry>();
                valsY.add(new Entry(countAura* 100 / events.size(),0));
                valsY.add(new Entry(countConvulsion * 100 / events.size(),1));
                valsY.add(new Entry(countContactRupture * 100 / events.size(),2));

                //creamos una lista de valores X
                ArrayList<String> valsX = new ArrayList<String>();
                valsX.add(getString(R.string.aura_crisis_simple));
                valsX.add(getString(R.string.convulsi_n_t_nico_cl_nica));
                valsX.add(getString(R.string.ruptura_de_contacto_crisis_compleja));

                ArrayList<Integer> colors = new ArrayList<Integer>();
                colors.add(getColor(R.color.aura));
                colors.add(getColor(R.color.convulsion));
                colors.add(getColor(R.color.contact_rupture));

                /*seteamos los valores de Y y los colores*/
                PieDataSet set1 = new PieDataSet(valsY,null);
                set1.setSliceSpace(3f);
                set1.setColors(colors);

                /*seteamos los valores de X*/
                PieData data = new PieData(valsX, set1);
                pieChart.setData(data);
                pieChart.highlightValues(null);
                pieChart.invalidate();
            }else {
                Toasty.success(DetailRegister.this, "Felicitaciones no tuviste episodios por el momento", Toast.LENGTH_LONG).show();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }
}
