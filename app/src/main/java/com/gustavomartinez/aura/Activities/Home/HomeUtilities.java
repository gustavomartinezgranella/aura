package com.gustavomartinez.aura.Activities.Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.ImageView;

import com.gustavomartinez.aura.Utils.Setting.Images;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;

public class HomeUtilities {
     /*
     * fija el contenido a compartir
     * */
    public static Intent setRecommendContent(String typeIntent, String message, String subject){
        Intent intent = new Intent(typeIntent);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        return intent;
    }

    public static void setImageProfile(Context context, ImageView profile){
        SharedPreferences preferences = context.getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
        if (profile!= null && preferences != null)
            Images.setImage(profile, preferences,context);
    }
}
