package com.gustavomartinez.aura.Activities.Contact;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Adapters.ContactAdapter;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Dao.Contact;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class Contacts extends AppCompatActivity {

    private ListView listView;
    private List<com.gustavomartinez.aura.Models.Contact> contacts = new ArrayList<>();
    private ContactAdapter contactAdapterList;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setTitle(getResources().getText(R.string.contactos_de_emergencia));

        listView = findViewById(R.id.list_view_contacts);
        adView = findViewById(R.id.adViewContacts);
        PublicityUtils.setPublicity(adView);


        long id = User.getIdUserActive(Contacts.this);
        contacts=Contact.getAllContacts(Contacts.this,id);

        contactAdapterList = new ContactAdapter(Contacts.this, R.layout.list_contacts, contacts);
        listView.setAdapter(contactAdapterList);
        registerForContextMenu(listView);

        if (contacts.isEmpty())
            Toasty.info(this, getString(R.string.no_hay_contactos_registrados), Toast.LENGTH_SHORT).show();
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    //metodos para personalizador el boton agregar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.contact_menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add_contact:
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.CALL_PHONE},
                                Permission.PERMISSION_CONTACT_AND_CALL_PHONE);
                    }else {
                        OlderVersionReadContacts();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission = null;
        int result = -1;
        switch (requestCode){
            case Permission.CONTACT_CODE :
                permission = permissions[0];
                result = grantResults[0];

                if (permission.equals(Manifest.permission.READ_CONTACTS)){
                    //comprobar si ha sido aceptado o denegado la peticion del permiso
                    if(result==PackageManager.PERMISSION_GRANTED){
                        //concedio permiso
                        Intent intent = new Intent(Contacts.this, NewContact.class);
                        startActivity(intent);
                    }else{
                        //no concedio el permiso
                        Toasty.info(Contacts.this, this.getString(R.string.need_permission_contacts), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                }
                break;
            case Permission.CALL_PHONE_CODE:
                permission = permissions[0];
                result = grantResults[0];

                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    //comprobar si ha sido aceptado o denegado la peticion del permiso
                    if(result==PackageManager.PERMISSION_GRANTED){
                        //concedio permiso
                    }else{
                        //no concedio el permiso
                        Toasty.info(Contacts.this, this.getString(R.string.need_permission_call), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                }
                break;
            case Permission.PERMISSION_CONTACT_AND_CALL_PHONE :

                for (int index=0; index<permissions.length; index++){
                    permission = permissions[index];
                    result = grantResults[0];

                    if (permission.equals(Manifest.permission.CALL_PHONE) || permission.equals(Manifest.permission.READ_CONTACTS)){
                        if (result == PackageManager.PERMISSION_GRANTED){
                            Intent intent = new Intent(Contacts.this, NewContact.class);
                            startActivity(intent);
                        }else{
                            //no concedio el permiso
                            Toasty.info(Contacts.this, this.getString(R.string.need_permission), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + getPackageName()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                        }
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    private boolean CheckPermission(String permission){
        int result = checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void OlderVersionReadContacts(){
        if (CheckPermission(Manifest.permission.READ_CONTACTS) && CheckPermission(Manifest.permission.CALL_PHONE)){
            Intent intent = new Intent(Contacts.this, NewContact.class);
            startActivity(intent);
        }else {
            Toasty.info(this, this.getString(R.string.cancel_permission), Toast.LENGTH_SHORT).show();
        }
    }
}
