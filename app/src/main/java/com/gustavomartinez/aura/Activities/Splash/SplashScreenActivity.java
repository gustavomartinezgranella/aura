package com.gustavomartinez.aura.Activities.Splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gustavomartinez.aura.Activities.Authentication.LoginActivity;
import com.gustavomartinez.aura.Activities.Home.Home;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;

public class SplashScreenActivity extends AppCompatActivity {

    private static boolean splashLoaded = false;

    private void verifiyAuthentication(boolean isLogged, String email_store, String pass){

        Intent intent;

        if (isLogged){
            intent = new Intent(SplashScreenActivity.this, Home.class);
            intent.putExtra("emailUser", email_store);
            intent.putExtra("pass", pass);
            intent.putExtra("isLogged", String.valueOf(isLogged));
            startActivity(intent);
        }else {
            intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            intent.putExtra("emailUser", email_store);
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
        final String email_store = preferences.getString("email", "");
        final String pass = preferences.getString("password","");
        final boolean isLogged = preferences.getBoolean("isLogged",false);

        if (!splashLoaded) {
            getSupportActionBar().hide();
            setContentView(R.layout.activity_splash_screen);
            int secondsDelayed = 3;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    Intent intent;
                    if (isLogged){
                        intent = new Intent(SplashScreenActivity.this, Home.class);
                        intent.putExtra("emailUser", email_store);
                        intent.putExtra("pass", pass);
                        intent.putExtra("isLogged", String.valueOf(isLogged));
                        startActivity(intent);
                    }else {
                        intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        intent.putExtra("emailUser", email_store);
                        startActivity(intent);
                    }

                    finish();
                }
            }, secondsDelayed * 500);

            splashLoaded = true;
        }
        else {
            verifiyAuthentication(isLogged, email_store, pass);
            finish();
        }

    }
}
