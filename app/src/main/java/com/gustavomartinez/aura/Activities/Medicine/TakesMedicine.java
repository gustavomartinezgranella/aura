package com.gustavomartinez.aura.Activities.Medicine;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Adapters.TakeMedicineAdapter;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.TakeMedicine;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class TakesMedicine extends AppCompatActivity {

    private ListView listView;
    private List<TakeMedicine> takesMedicines = new ArrayList<>();
    private TakeMedicineAdapter takeMedicineAdapter;
    private AdView adView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_takes_medicine);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(getResources().getText(R.string.dosis_realizadas));
        listView = findViewById(R.id.list_view_take_medicine);
        adView = findViewById(R.id.adTakeMedicine);
        PublicityUtils.setPublicity(adView);
        long id = User.getIdUserActive(TakesMedicine.this);

        takesMedicines = com.gustavomartinez.aura.Dao.TakeMedicine.getAllTakesMedicines(TakesMedicine.this, id);

        takeMedicineAdapter = new TakeMedicineAdapter(TakesMedicine.this, R.layout.list_take_medicine, takesMedicines);
        listView.setAdapter(takeMedicineAdapter);
        registerForContextMenu(listView);

        if (takesMedicines.isEmpty())
            Toasty.info(this, getString(R.string.no_hay_tomas_realizadas), Toast.LENGTH_SHORT).show();

    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

}
