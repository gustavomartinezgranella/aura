package com.gustavomartinez.aura.Activities.Event;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Db.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class NewEvent extends AppCompatActivity {

    private EditText description;
    private EditText medicinePreventive;
    private TextView charsAvailables;
    private TextView spanEvent;
    private RadioGroup radioGroup;
    private SeekBar seek;
    private Button cancel;
    private Button saveEvent;
    private String typeEvent="";
    private AdView adView;
    //constants
    private final int LIMIT_COUNT_CHARS_DESCRIPTION =200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        setTitle(getResources().getText(R.string.add_event));
        this.init();
        setListeners();
    }

    private void setListeners() {
        saveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validate();
                    List<User> users = com.gustavomartinez.aura.Dao.User.getUserActive(NewEvent.this);

                    if(users!=null && users.size()>0){
                        long id_user = users.get(0).getId();
                        if(description.getText().toString().length()>0){

                            com.gustavomartinez.aura.Dao.Event.insertEvent(NewEvent.this,medicinePreventive.getText().toString()
                                    ,typeEvent,seek.getProgress(),description.getText().toString(),id_user);
                        }else{
                            com.gustavomartinez.aura.Dao.Event.insertEvent(NewEvent.this,medicinePreventive.getText().toString()
                                    ,typeEvent,seek.getProgress(),users.get(0).getId());
                        }

                        Intent intent = new Intent(NewEvent.this, Events.class);
                        startActivity(intent);
                    }else{
                        Toasty.info(NewEvent.this, getString(R.string.not_exist_profle_asociated_you_user), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicinePreventive.setText("");
                seek.setProgress(0);
                spanEvent.setText(R.string.init_progress);
                description.setText("");
                charsAvailables.setText(R.string._0_255);
                radioGroup.clearCheck();
            }
        });


        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charsAvailables.setText((s.length())+"/200");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //configure a listener to get obtain the event's count minutes
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                spanEvent.setText(R.string.init_progress);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                spanEvent.setText(new StringBuilder().append(progress).append(getString(R.string.minutes)).toString());
            }
        });

        //configure a listener to obtain the event's type
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_aura :
                        typeEvent = getString(R.string.aura_crisis_simple);
                        break;
                    case R.id.rb_convulsion :
                        typeEvent = getString(R.string.convulsi_n_t_nico_cl_nica);
                        break;
                    default:
                        typeEvent= getString(R.string.ruptura_de_contacto_crisis_compleja);
                }
            }
        });
    }

    private void validate() throws Exception {

        if (this.medicinePreventive.getText().toString().equals("") || this.medicinePreventive.getText() == null){
            Toasty.warning(NewEvent.this, getString(R.string.should_enter_preventive_medication), Toast.LENGTH_SHORT).show();
            throw new Exception("Debe ingresar su medicamento preventivo");
        }else{
            if (!(typeEvent.equals(getString(R.string.aura_crisis_simple)) || typeEvent.equals(getString(R.string.convulsi_n_t_nico_cl_nica)) || typeEvent.equals(getString(R.string.ruptura_de_contacto_crisis_compleja)))){
                Toasty.warning(NewEvent.this, getString(R.string.enter_type_event), Toast.LENGTH_SHORT).show();
                throw new Exception("Debe ingresar el tipo de registro");
            }else{
                if (!(seek.getProgress()>0 && seek.getProgress()<=60)){
                    Toasty.warning(NewEvent.this, getString(R.string.duration_0_60), Toast.LENGTH_SHORT).show();
                    throw new Exception("Debe ingresar el tipo de registro");
                }else{
                    if (description.getText().toString().length()>200){
                        Toasty.warning(NewEvent.this, getString(R.string.description_no_than_200), Toast.LENGTH_SHORT).show();
                        throw new Exception("Debe ingresar el tipo de registro");
                    }
                }
            }
        }

    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    private void init(){
        medicinePreventive = findViewById(R.id.et_medicine_preventive);
        description = findViewById(R.id.textArea_information);
        charsAvailables = findViewById(R.id.charsAvaible);
        spanEvent = findViewById(R.id.minutesEvents);
        radioGroup = findViewById(R.id.rg_type_event);
        seek = findViewById(R.id.seekBar1);
        description.setBackgroundResource(R.drawable.edittext_add_event);
        cancel = findViewById(R.id.btnCancellEvent);
        saveEvent = findViewById(R.id.btnSaveEvent);
        adView = findViewById(R.id.adNewEvents);
        PublicityUtils.setPublicity(adView);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getActionBar().setDisplayShowHomeEnabled(true);
        }
    }
}
