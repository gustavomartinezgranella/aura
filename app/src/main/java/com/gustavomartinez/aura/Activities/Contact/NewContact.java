package com.gustavomartinez.aura.Activities.Contact;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.gustavomartinez.aura.Activities.Authentication.LoginActivity;
import com.gustavomartinez.aura.Adapters.ContactAdapter;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.Models.Contact;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Contacts.ContactsUtils;
import com.gustavomartinez.aura.Utils.Permissions.Permission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import es.dmoral.toasty.Toasty;

public class NewContact extends AppCompatActivity {

    private ListView listView;
    private List<Contact> contacts = new ArrayList<>();
    private ContactAdapter contactAdapter;
    private SearchView searchPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_new_contact);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getActionBar().setDisplayShowHomeEnabled(true);
            }
            setTitle(getResources().getText(R.string.agregar_contacto));

            listView = findViewById(R.id.list_view__new_contacts);
            searchPattern = findViewById(R.id.sv_filter_contacts);

            Runnable runnable = () -> contacts = fetchContacts();
            runnable.run();

            contactAdapter = new ContactAdapter(NewContact.this, R.layout.list_new_contact, contacts);
            listView.setAdapter(contactAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Contact c = (Contact) parent.getAdapter().getItem(position);
                    Contact contactToInsert = contacts.get(getPositionByNameAndNumber(c.getFullname(), c.getNumber()));

                    if (contactToInsert!=null){
                        long id_user = User.getIdUserActive(NewContact.this);
                        com.gustavomartinez.aura.Dao.Contact.insertContact(NewContact.this
                                ,contactToInsert.getFullname(),contactToInsert.getNumber(),id_user);
                    }else{
                        Toasty.error(NewContact.this, getString(R.string.no_get_contact_to_insert), Toast.LENGTH_SHORT).show();
                    }
                    Intent intent = new Intent(NewContact.this, Contacts.class);
                    startActivity(intent);

                }
            });

            searchPattern.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    contactAdapter.obtainFilter(fetchContacts()).filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    if (query.length()==0)
                        contactAdapter.obtainFilter(fetchContacts()).filter(query);
                    return false;
                }
            });

            registerForContextMenu(listView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    private List<Contact> fetchContacts(){

        List<Contact> contact = new ArrayList<>();
        Set<Contact> contactSet= new HashSet<>();

        verifyPermissions();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            Set<Contact> setContactInternals = ContactsUtils.processInternalContacts(NewContact.this);
            Set<Contact> setContactSim = ContactsUtils.processSimContacts(NewContact.this);

            if (setContactInternals!=null && !setContactInternals.isEmpty())
                contactSet.addAll(setContactInternals);

            if (setContactSim!=null && !setContactSim.isEmpty())
                contactSet.addAll(setContactSim);
        }

        contact.addAll(contactSet);
        return contact;
    }

    //metodos para personalizador el boton agregar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.new_contact_menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add_new_contact:
                try {
                    Intent intent = new Intent(NewContact.this, FactoryContact.class);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public int getPositionByNameAndNumber(String name, String phoneNumber){
        for (int index=0 ; index<contacts.size(); index++){
            if (name.equals(contacts.get(index).getFullname()) && phoneNumber.equals(contacts.get(index).getNumber())){
                return index;
            }
        }

        return -1;
    }

    private void verifyPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 200);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS}, 200);
        }
    }

}
