package com.gustavomartinez.aura.Activities.Home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.gustavomartinez.aura.Activities.Contact.Contacts;
import com.gustavomartinez.aura.Activities.Event.Events;
import com.gustavomartinez.aura.Activities.Authentication.LoginActivity;
import com.gustavomartinez.aura.Activities.Medicine.ListMedicine;
import com.gustavomartinez.aura.Activities.Settings.SettingActivity;
import com.gustavomartinez.aura.Activities.Medicine.TakesMedicine;
import com.gustavomartinez.aura.Dao.User;
import com.gustavomartinez.aura.R;
import com.gustavomartinez.aura.Utils.Authentication.UtilsAuthentication;
import com.gustavomartinez.aura.Utils.Localization.LocalizationPoint;
import com.gustavomartinez.aura.Utils.Messages.SenderMessage;
import com.gustavomartinez.aura.Utils.Messages.Message;
import com.gustavomartinez.aura.Utils.Permissions.Permission;
import com.gustavomartinez.aura.Utils.Publicity.PublicityUtils;

import java.util.List;

import es.dmoral.toasty.Toasty;

import static android.os.Build.VERSION_CODES.M;
import static com.gustavomartinez.aura.Activities.Home.HomeUtilities.setRecommendContent;

public class Home extends AppCompatActivity {


    private Button btnLogout;
    private Button btnRecommendApp;
    private Button btnSettings;
    private Button btnEvents;
    private Button btnMedicine;
    private Button btnEmergencyContacts;
    private Button btnWarnCrisis;
    private Button btnTakeMedicine;
    private ImageView profile;
    private String email_user = null;
    private String password_user = null;
    private String isLogged = null;
    private AdView mAdView;

    //propiedades para la geolocalizacion
    private LocationManager locationManager;
    private LocationListener locationListener;
    private double latitud;
    private double longitud;
    private Location lastKnownLocation;

    private void setProfileFields() {

        if (getIntent() != null) {
            email_user = getIntent().getStringExtra("emailUser");
            password_user = getIntent().getStringExtra("pass");
            isLogged = getIntent().getStringExtra("isLogged");
        }

        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);

        if (email_user == null)
            email_user = preferences.getString("email", "");
        if (password_user == null)
            password_user = preferences.getString("pass", "");
        if (isLogged == null)
            isLogged = String.valueOf(preferences.getBoolean("isLogged", true));
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateLocalization();
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateLocalization();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            getSupportActionBar().hide();
            setContentView(R.layout.activity_home);
            initComponents();
            setListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setListeners() {

        btnMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, ListMedicine.class);
                startActivity(intent);
            }
        });

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateLocalization();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };

        updateLocalization();

        btnWarnCrisis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocalization();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if ((ContextCompat.checkSelfPermission(Home.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
                            || (ContextCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Home.this,
                                Manifest.permission.SEND_SMS)) {
                            requestChangeStatusPermission(Manifest.permission.SEND_SMS, Home.this);
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(Home.this
                                    , new String[]{Manifest.permission.SEND_SMS, Manifest.permission.ACCESS_FINE_LOCATION},
                                    Permission.SEND_MESSAGE_AND_LOCATION_CODE);
                        }
                    } else {

                        updateLocalization();
                        //sendMessage();
                        Message.sendMessage(Home.this, lastKnownLocation, latitud, longitud);
                    }
                } else {

                    if (ContextCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestChangeStatusPermission(getString(R.string.enable_gps_for_location), Home.this);
                    } else {
                        updateLocalization();
                        Message.sendMessage(Home.this, lastKnownLocation, latitud, longitud);
                    }
                }
            }
        });

        btnTakeMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, TakesMedicine.class);
                startActivity(intent);
            }
        });

        btnEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Events.class);
                startActivity(intent);
            }
        });

        btnEmergencyContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Contacts.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<com.gustavomartinez.aura.Db.User> users = User.getUserActive(Home.this);

                if (users != null && users.size() > 0) {
                    com.gustavomartinez.aura.Db.User user = users.get(0);
                    if (isLogged.equals("true") && user.getLogged()) {
                        User.changeStatusLogged(Home.this, user, false);
                        SharedPreferences preferences = getSharedPreferences(UtilsAuthentication.NAME_FILE_USER_PREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("isLogged", false);
                        editor.apply();

                        Intent intent = new Intent(Home.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        btnRecommendApp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //verifico la version de android del dispositivo
                if (Build.VERSION.SDK_INT >= M) {
                    //verifico si tiene el permiso
                    if (checkPermission(Manifest.permission.INTERNET)) {
                        Intent intent = setRecommendContent(Intent.ACTION_SEND
                                , Home.this.getString(R.string.message_recommend)
                                , Home.this.getString(R.string.subject_recommend_app));
                        //verifico el permiso
                        if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.INTERNET)
                                != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(intent);
                    } else {
                        hasNotPermission(Manifest.permission.INTERNET
                                , Permission.INTERNET_CODE, Home.this.getString(R.string.recommend_app), Home.this);
                    }
                } else {
                    proccessPermissionToOlderVersion(Home.this.getString(R.string.help_me_to_recommend)
                            , Home.this.getString(R.string.message_recommend)
                            , Home.this.getString(R.string.subject_recommend_app));
                }
            }

        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Home.this, SettingActivity.class);
                    startActivity(intent);
                }
        });
    }

    private void initComponents() {
        this.setProfileFields();
        btnLogout = findViewById(R.id.btnLogout);
        btnRecommendApp = findViewById(R.id.btnRecommend);
        btnSettings = findViewById(R.id.btnSettings);
        btnEmergencyContacts = findViewById(R.id.btnEmergencyContacts);
        profile = findViewById(R.id.iv_profile);
        btnEvents = findViewById(R.id.btnEvents);
        btnWarnCrisis = findViewById(R.id.btnDanger);
        btnMedicine = findViewById(R.id.btnMedicine);
        HomeUtilities.setImageProfile(Home.this, profile);
        mAdView = findViewById(R.id.adView);
        PublicityUtils.setPublicity(mAdView);
        btnTakeMedicine = findViewById(R.id.btnTakeMedicine);

        if (!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)){
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                ActivityCompat.requestPermissions(Home.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Permission.LOCALIZATION_FINE);
            }else {
                requestChangeStatusPermission(getString(R.string.enable_gps_for_location),Home.this);
            }
        }

        if (!checkPermission(Manifest.permission.SEND_SMS)){
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                ActivityCompat.requestPermissions(Home.this, new String[]{Manifest.permission.SEND_SMS}, Permission.SEND_MESSAGE_AND_LOCATION_CODE);
            }else {
                requestChangeStatusPermission(getString(R.string.enable_sms),Home.this);
            }
        }

    }


    private void hasNotPermission(String permission, int codePermission, String messageRequestPermission, Context context){
        if (Build.VERSION.SDK_INT >= M){
            //verifico si debo mostrar el popup para habilitar permiso
            if(shouldShowRequestPermissionRationale(permission)){
                //no se le ha preguntado aun
                requestPermissions(new String[]{permission}, codePermission);
            }else{
                //aplico el comportamiento para que active el permiso
                requestChangeStatusPermission(messageRequestPermission, context);
            }
        }
    }

    /*
    * requestChangeStatusPermission
    * @param message, lo que debe informar
    * @param context sobre que contexto debe ejecutarse
    *
    * genera el comportamiento que debe ejecutarse si hay que cambiar un permiso
    * */
    private void requestChangeStatusPermission(String message, Context context){
        try {
            Toasty.info(context, message, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", getPackageName(), null));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
    * proccessPermissionToOlderVersion
    * implementa el procesamiento en el caso de que se utilice version de Android inferior a la version 6
    * */
    private void proccessPermissionToOlderVersion(String title, String message, String subject){
        Intent intent = setRecommendContent(Intent.ACTION_SEND, message, subject);
        if(checkPermission(Manifest.permission.INTERNET)){
            startActivity(Intent.createChooser(intent, title));
        }
    }


    /*
    * checkPeremission verifica si el permiso solicitado esta aprobado
    *
    * @param permission
    * @return boolean
    * */
    private boolean checkPermission(String permission){
        int codeStatus = this.checkCallingOrSelfPermission(permission);
        return codeStatus == PackageManager.PERMISSION_GRANTED;
    }


    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }


    /*
    * se sobreescribe este metodo para personalizar el comportamiento dependiendo del permiso utilizado
    * */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permission;
        int result;
        switch (requestCode) {

            case Permission.LOCALIZATION_FINE:

                if (permissions.length>0){

                    permission = permissions[0];
                    result = grantResults[0];

                    if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {

                        //COMPROBAR SI EL PERMISO ESTA DENEGADO O ACEPTADO EL PERMISO
                        if (result == PackageManager.PERMISSION_GRANTED) {
                            //concedio  el permiso
                            startListeningCoordinates();
                        }else{
                            //no concedio el permiso
                            Toasty.info(Home.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            case Permission.SEND_MESSAGE_AND_LOCATION_CODE:

                for (int index=0; index<permissions.length; index++){
                    permission = permissions[index];
                    result = grantResults[0];
                    int counterPermission = 0;
                    if (permission.equals(Manifest.permission.SEND_SMS) || permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {

                        //COMPROBAR SI EL PERMISO ESTA DENEGADO O ACEPTADO EL PERMISO
                        if (result == PackageManager.PERMISSION_GRANTED) {
                            //concedio  el permiso
                            if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
                                || (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)   ) {
                                Toasty.info(Home.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
                                return;
                            }else{
                                counterPermission++;

                                if (counterPermission == 2) {
                                    if (lastKnownLocation != null && latitud != 0.0 && longitud != 0.0){
                                        LocalizationPoint localizationPoint = LocalizationPoint.getLocalizationPoint();
                                        localizationPoint.setLongitud(longitud);
                                        localizationPoint.setLatitud(latitud);
                                        SenderMessage senderMessage = new SenderMessage(Home.this, latitud, longitud);
                                        senderMessage.sendMessage();
                                    }
                                }else{
                                    continue;
                                }
                            }
                        }
                    }else{
                        //no concedio el permiso
                        Toasty.info(Home.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
                    }

                }
                break;

            case Permission.INTERNET_CODE:

                if (permissions.length>0){
                    permission = permissions[0];
                    result = grantResults[0];

                    if (permission.equals(Manifest.permission.INTERNET)) {

                        //COMPROBAR SI EL PERMISO ESTA DENEGADO O ACEPTADO EL PERMISO
                        if (result == PackageManager.PERMISSION_GRANTED) {
                            //concedio  el permiso
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, this.getString(R.string.message_recommend));
                            intent.putExtra(Intent.EXTRA_TITLE, this.getString(R.string.help_me_to_recommend));
                            intent.putExtra(Intent.EXTRA_TITLE, this.getString(R.string.help_me_to_recommend));
                            intent.setType("text/plain");
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            startActivity(intent);
                        }else{
                            //no concedio el permiso
                            Toasty.info(Home.this, this.getString(R.string.cancel_permission), Toast.LENGTH_LONG).show();
                        }

                    }
                }

                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void startListeningCoordinates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
        }
    }

    public void updateLocationInfo(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
        LocalizationPoint localizationPoint = LocalizationPoint.getLocalizationPoint();
        localizationPoint.setLongitud(longitud);
        localizationPoint.setLatitud(latitud);
    }

    private void updateLocalization(){
        if (ContextCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Home.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Permission.LOCALIZATION_FINE);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
            lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocation != null) {
                updateLocationInfo(lastKnownLocation);
            }else {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);
                lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if(lastKnownLocation!=null){
                    updateLocationInfo(lastKnownLocation);
                }
            }
        }
    }
}